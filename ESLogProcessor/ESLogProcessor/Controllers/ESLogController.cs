﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using ESLogProcessor.Models;
using ESLogProcessor.Business;

namespace ESLogProcessor.Controllers
{
    public class ESLogController : ApiController
    {
        
        [Route("api/ESLog/SearchEsLog")]
        [HttpPost]
        public Response SearchEsLog([FromBody]SearchParam param)
        {
            ESLogBussiness eb = new ESLogBussiness();
            Response rsp = new Response();
            rsp.resultCode = 1;
            rsp.resultMsg = "";

            if (param == null)
            {
                rsp.resultMsg = "请求格式错误";
                return rsp;
            }

            try
            {
                rsp.resultData = eb.getEslogs(param.pageid, param.count,param.refpageid, param.ubtKeys,param.dataKeys);
                if (rsp.resultData.Count == 0)
                {
                    rsp.resultCode = 1;
                    rsp.resultMsg = "查无结果";
                }
                else
                    rsp.resultCode = 0;
            }
            catch(Exception e)
            {
                rsp.resultMsg = e.Message;
            }
            
            return rsp;
        }
        
    }

    public class SearchParam
    {
        public string pageid { get; set; }
        public int count { get; set; }
        public string refpageid { get; set; }
        public List<UbtKey> ubtKeys { get; set; }
        public List<DataKey> dataKeys { get; set; }
    }

    
}
