﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESLogProcessor.Models
{
    public class Response
    {
        public int resultCode { get; set; }
        public string resultMsg { get; set; }
        public List<ResultData> resultData { get; set; }
    }

    public class ResultData
    {
        public string pvid { get; set; }
        public string sid { get; set; }
        public string vid { get; set; }
        public string type { get; set; }
        public string clientID { get; set; }
        public long startDate { get; set; }
        public long endDate { get; set; }
        public List<ExtraKey> extraKeys { get; set; }
    }

    public class ExtraKey
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}