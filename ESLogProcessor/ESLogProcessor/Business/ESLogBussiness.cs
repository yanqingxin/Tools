﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ESLogProcessor.Models;
using Newtonsoft.Json.Linq;

namespace ESLogProcessor.Business
{
    public class ESLogBussiness
    {
        string traceLogUrl = "http://fftlog.product.flight.ctripcorp.com/Microscope/LogSearch/UBTAggSearchAjax?custom=";
        int COUNT = 0;
        List<ResultData> ResultDataList = new List<ResultData>();

        public List<ResultData> getEslogs(string pageid, int count, string refPageid,List<UbtKey> ubtKeys,List<DataKey> dataKeys)
        {
            COUNT = count;

            int days = 6;
            string builtinTimestamp = string.Empty;
            string extraParam = string.Empty;
            foreach (UbtKey ubtkey in ubtKeys)
            {
                extraParam += string.Format(" and {0}='{1}'", ubtkey.key, ubtkey.value);
            }

            for (int i = 1; i < days; i++)
            {
                string date = DateTime.Today.AddDays(-i).ToString("yyyyMMdd");
                string sql = string.Format("select * from tracelog-{0} where context_page='{1}' {2} {3} order by builtinTimestamp desc limit {4}", date, pageid,extraParam, builtinTimestamp == string.Empty ? "" : string.Format("and builtinTimestamp <'{0}'", builtinTimestamp), count * 10 > 5000 ? 5000 : count * 2);
                
                string request = traceLogUrl + HttpUtility.UrlEncode(sql);
                string response = HttpGet(request);

                if (response == "")
                    continue;

                JObject jResult = JObject.Parse(response);
                List<Task> taskList = new List<Task>();
                if (jResult["result"] != null && jResult["result"]["hits"] != null && jResult["result"]["hits"]["hits"] != null && jResult["result"]["hits"]["hits"].Count() != 0)
                {
                    for (int j = 0; j < jResult["result"]["hits"]["hits"].Count(); j++)
                    {
                        ResultData resultData = new ResultData();

                        JToken pvid = jResult["result"]["hits"]["hits"][j]["_source"]["context_pvid"];
                        if (pvid == null || pvid.ToString().Trim() == "")
                            continue;
                        resultData.pvid = pvid.ToString();

                        JToken sid = jResult["result"]["hits"]["hits"][j]["_source"]["context_sid"];
                        if (sid == null || sid.ToString().Trim() == "")
                            continue;
                        resultData.sid = sid.ToString();

                        JToken vid = jResult["result"]["hits"]["hits"][j]["_source"]["context_vid"];
                        if (vid == null || vid.ToString().Trim() == "")
                            continue;
                        resultData.vid = vid.ToString();

                        JToken type = jResult["result"]["hits"]["hits"][j]["_source"]["context_type"];
                        if (type == null || type.ToString().Trim() == "")
                            continue;
                        resultData.type = type.ToString();

                        JToken clientId = jResult["result"]["hits"]["hits"][j]["_source"]["context_clientId"];
                        if (clientId == null || clientId.ToString().Trim() == "")
                            continue;
                        resultData.clientID = clientId.ToString();

                        JToken ts = jResult["result"]["hits"]["hits"][j]["_source"]["context_ts"];
                        if (ts == null || ts.ToString().Trim() == "")
                            continue;
                        resultData.startDate = ConvertDateTimeLong(ConvertDateTimeLong(long.Parse(ts.ToString())).AddMinutes(-3));
                        resultData.endDate = ConvertDateTimeLong(ConvertDateTimeLong(long.Parse(ts.ToString())).AddMinutes(2));

                        JToken custom_data = jResult["result"]["hits"]["hits"][j]["_source"]["custom_data"]["data"];
                        if (custom_data == null)
                            continue;
                        JObject data = null;
                        try
                        {
                            data = JObject.Parse(custom_data.ToString());
                        }
                        catch
                        {
                            continue;
                        }                        

                        builtinTimestamp = jResult["result"]["hits"]["hits"][j]["_source"]["builtinTimestamp"].ToString();
                        if (refPageid != null && refPageid.Trim() != "")
                        {
                            Task eachTask = null;
                            string _date = date;
                            string _clientId = clientId.ToString();
                            string _buildinTimeStamp = builtinTimestamp;
                            eachTask = Task.Factory.StartNew(() =>
                            {
                                getRefPageData(_date, refPageid, _clientId, _buildinTimeStamp,dataKeys);
                            });

                            taskList.Add(eachTask);

                            if ((j + 1) % 20 == 0 || jResult["result"]["hits"]["hits"].Count() == j + 1)//每次处理20个
                            {
                                Task.WaitAll(taskList.ToArray());
                                taskList.Clear();
                            }
                        }
                        else
                        {
                            List<ExtraKey> eks = new List<ExtraKey>();
                            bool noFound = false;
                            foreach (DataKey dk in dataKeys)
                            {
                                ExtraKey ek = new ExtraKey();
                                ek.key = dk.key;
                                JToken dkPath = data;
                                foreach (string path in dk.path)
                                {
                                    dkPath = dkPath[path];
                                    if (dkPath == null)
                                    {
                                        noFound = true;
                                        break;
                                    }
                                }
                                if (dkPath == null)
                                {
                                    noFound = true;
                                    break;
                                }
                                else
                                    ek.value = dkPath.ToString();

                                eks.Add(ek);
                            }

                            if (noFound)
                                continue;
                            resultData.extraKeys = eks;


                            if (resultData != null && !ResultDataList.Exists(e => e.pvid == resultData.pvid && e.sid == resultData.sid && e.type == resultData.type && e.vid == resultData.vid || e.extraKeys.Count>0 && extraKeysEqual(e.extraKeys,resultData.extraKeys)))
                                ResultDataList.Add(resultData);
                        }
                        if (ResultDataList.Count >= COUNT)
                            return ResultDataList;
                    }
                }
                else
                {
                    builtinTimestamp = string.Empty;
                    continue;
                }
                if(builtinTimestamp!=string.Empty)
                    i--;
            }

            return ResultDataList; 
            
        }

        private bool extraKeysEqual(List<ExtraKey> extraKeys1, List<ExtraKey> extraKeys2)
        {
            if (extraKeys1 == null && extraKeys2 == null)
                return true;
            else if (extraKeys1 == null || extraKeys2 == null)
                return false;
            else if (extraKeys1.Count != extraKeys2.Count)
                return false;
            else
            {
                for (int i = 0; i < extraKeys1.Count; i++)
                {
                    if (extraKeys1[i].key != extraKeys2[i].key || extraKeys1[i].value != extraKeys2[i].value)
                        return false;
                }
                return true;
            }
        }

        private void getRefPageData(string date, string refPageid,string clientid, string builtinTimestamp,List<DataKey> dataKeys)
        {
            lock (ResultDataList)
            {
                if (ResultDataList.Count >= COUNT)
                    return;
            }

            ResultData rd = new ResultData();

            string sql = string.Format("select * from tracelog-{0} where context_page='{1}' and context_clientId = '{2}' and builtinTimestamp < '{3}' order by builtinTimestamp desc limit 1", date, refPageid, clientid, builtinTimestamp);

            string request = traceLogUrl + HttpUtility.UrlEncode(sql);
            string response = HttpGet(request);

            if (response == "")
                return;

            JObject jResult = JObject.Parse(response);
            if (jResult["result"] != null && jResult["result"]["hits"] != null && jResult["result"]["hits"]["hits"] != null && jResult["result"]["hits"]["hits"].Count() != 0)
            {
                for (int j = 0; j < jResult["result"]["hits"]["hits"].Count(); j++)
                {
                    ResultData resultData = new ResultData();

                    JToken pvid = jResult["result"]["hits"]["hits"][j]["_source"]["context_pvid"];
                    if (pvid == null || pvid.ToString().Trim() == "")
                        continue;
                    resultData.pvid = pvid.ToString();

                    JToken sid = jResult["result"]["hits"]["hits"][j]["_source"]["context_sid"];
                    if (sid == null || sid.ToString().Trim() == "")
                        continue;
                    resultData.sid = sid.ToString();

                    JToken vid = jResult["result"]["hits"]["hits"][j]["_source"]["context_vid"];
                    if (vid == null || vid.ToString().Trim() == "")
                        continue;
                    resultData.vid = vid.ToString();

                    JToken type = jResult["result"]["hits"]["hits"][j]["_source"]["context_type"];
                    if (type == null || type.ToString().Trim() == "")
                        continue;
                    resultData.type = type.ToString();

                    JToken clientId = jResult["result"]["hits"]["hits"][j]["_source"]["context_clientId"];
                    if (clientId == null || clientId.ToString().Trim() == "")
                        continue;
                    resultData.clientID = clientId.ToString();

                    JToken ts = jResult["result"]["hits"]["hits"][j]["_source"]["context_ts"];
                    if (ts == null || ts.ToString().Trim() == "")
                        continue;
                    resultData.startDate = ConvertDateTimeLong(ConvertDateTimeLong(long.Parse(ts.ToString())).AddMinutes(-3));
                    resultData.endDate = ConvertDateTimeLong(ConvertDateTimeLong(long.Parse(ts.ToString())).AddMinutes(2));

                    JToken custom_data = jResult["result"]["hits"]["hits"][j]["_source"]["custom_data"]["data"];
                    if (custom_data == null)
                        continue;
                    JObject data = null;
                    try
                    {
                        data = JObject.Parse(custom_data.ToString());
                    }
                    catch
                    {
                        continue;
                    }

                    List<ExtraKey> eks = new List<ExtraKey>();
                    bool noFound = false;
                    foreach (DataKey dk in dataKeys)
                    {
                        ExtraKey ek = new ExtraKey();
                        ek.key = dk.key;
                        JToken dkPath = data;
                        foreach (string path in dk.path)
                        {
                            dkPath = dkPath[path];
                            if (dkPath == null)
                            {
                                noFound = true;
                                break;
                            }
                        }
                        if (dkPath == null)
                        {
                            noFound = true;
                            break;
                        }
                        else
                            ek.value = dkPath.ToString();

                        eks.Add(ek);
                    }

                    if (noFound)
                        continue;
                    resultData.extraKeys = eks;

                    lock (ResultDataList)
                    {
                        if (ResultDataList.Count >= COUNT)
                            return;
                        else if (resultData != null && !ResultDataList.Exists(e => e.pvid == resultData.pvid && e.sid == resultData.sid && e.type == resultData.type && e.vid == resultData.vid || e.extraKeys.Count > 0 && extraKeysEqual(e.extraKeys, resultData.extraKeys)))
                            ResultDataList.Add(resultData);
                    }
                }
            }
            
            return;
        }

        /// <summary>
        /// http请求方法
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="postDataStr"></param>
        /// <returns></returns>
        private static string HttpGet(string Url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "GET";
            //request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";

            try
            {
                Thread.Sleep(1000);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //response.Cookies = cookie.GetCookies(response.ResponseUri);
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                string retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();

                return retString;

            }
            catch (WebException e)
            {
                return "";
            }
        }

        /// <summary>  
        /// DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time"> DateTime时间格式</param>  
        /// <returns>Unix时间戳格式</returns>  
        internal static long ConvertDateTimeLong(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000;   //除10000调整为13位      
            return t;
        }


        /// <summary>  
        /// DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time"> DateTime时间格式</param>  
        /// <returns>Unix时间戳格式</returns>  
        internal static DateTime ConvertDateTimeLong(long timeStamp)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            return new DateTime(timeStamp * 10000 + startTime.Ticks);
        }
    }
    public class UbtKey
    {
        public string key { get; set; }
        public string value { get; set; }
    }
    public class DataKey
    {
        public string key { get; set; }
        public List<string> path { get; set; }
    }
}