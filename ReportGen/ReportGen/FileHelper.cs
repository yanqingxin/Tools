﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGen
{
    class FileHelper
    {
        public void Process(string filePath)
        {
            string newFilePath = cloneFile(filePath);
        }

        private string cloneFile(string filePath)
        {
            string newFilePath = Path.GetDirectoryName(filePath)+"\\" + Path.GetFileNameWithoutExtension(filePath) + string.Format("_{0:yyyyMMddHHmmss}", DateTime.Now) + Path.GetExtension(filePath);
            File.Copy(filePath, newFilePath, true);
            return newFilePath;
        }
    }
}
