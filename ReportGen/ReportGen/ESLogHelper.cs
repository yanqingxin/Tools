﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ReportGen
{
    class ESLogHelper
    {
        static string logURL = "http://fftlog.product.flight.ctripcorp.com/Microscope/LogSearch/UBTAggSearchAjax";

        

        public int GetCount(string sql,string dateTime)
        {            
            string date = Regex.Match(dateTime, @"\d+/\d+/\d+").Value;
            if (date.Trim() == "")
                return -1;
            string timespan = Regex.Match(dateTime, @"\d+:\d+:\d+-\d+:\d+:\d+").Value;
            string startTime = string.Empty;
            string endTime = string.Empty;
            if (timespan.Trim() != string.Empty)
            {
                startTime = timespan.Split('-')[0];
                endTime = timespan.Split('-')[1];
            }
            sql = Regex.Replace(sql, @"-\d{8}", string.Format("-{0:yyyyMMdd}", DateTime.Parse(date)));
            

            if (startTime != string.Empty && endTime != string.Empty)
            {
                DateTime _startTime = DateTime.Parse(date + " " + startTime);
                DateTime _endTime = DateTime.Parse(date + " " + endTime);
                sql += string.Format(" and @timestamp > '{0:yyyy-MM-ddTHH:mm:ss}+0800' and @timestamp < '{1:yyyy-MM-ddTHH:mm:ss}+0800'", _startTime, _endTime);
            }

            string requestBody = HttpUtility.UrlEncode(sql);
            requestBody = string.Format("custom={0}", requestBody);

            string rsp = HttpPost(logURL, requestBody, "application/x-www-form-urlencoded");
            string valueItem = Regex.Match(rsp, @"""value"":\d+").Value;
            string value = Regex.Match(valueItem, @"\d+").Value;
            if (value.Trim() == string.Empty)
                return -1;
            else
                return int.Parse(value);
        }


        /// <summary>
        /// http请求方法
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="postDataStr"></param>
        /// <returns></returns>
        public string HttpPost(string Url, string postDataStr, string ContentType = "", string encoding = "gb2312")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";
            request.ContentType = "application/xml";
            if (ContentType != "")
                request.ContentType = ContentType;

            request.UserAgent = "Fiddler";
            Stream myRequestStream = request.GetRequestStream();
            StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding(encoding));
            myStreamWriter.Write(postDataStr);
            myStreamWriter.Close();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //response.Cookies = cookie.GetCookies(response.ResponseUri);
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding(encoding));
                string retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();

                return retString;

            }
            catch (WebException e)
            {
                return "";
            }
        }
    }
}
