﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;

namespace ReportGen
{

    public partial class ReportGenForm : Form
    {
        string filePath = string.Empty;
        Excel.Application _Excel = null;

        public ReportGenForm()
        {
            InitializeComponent();
        }

        private void OpenBtn_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            filePathTxt.Text = openFileDialog.FileName;
        }

        private void ProcessBtn_Click(object sender, EventArgs e)
        {
            //拿到了文件路径
            if (filePathTxt.Text.Trim() == string.Empty)
            {
                MessageBox.Show("请选择文件！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!File.Exists(filePathTxt.Text))
            {
                MessageBox.Show("路径文件不存在，请检查！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
           
            string path = filePathTxt.Text.Trim();
            ReadExcel(path);
            //writeExcel(path);
        }

        
        //初始化excel表格程序,需要吧excel进程全关闭
        private void initailExcel()
        {

            /*
            foreach (var item in Process.GetProcesses())
            {
                if (item.ProcessName == "EXCEL")
                {
                    object obj = Marshal.GetActiveObject("Excel.Application");//引用已在執行的Excel  
                    _Excel = obj as Excel.Application;
                    KillExcel(_Excel);
                }
            }*/

            this._Excel = new Excel.Application();
            this._Excel.Visible = true;
        }


        //读取Excel表格
        private void ReadExcel(string filePath) {
            
            Excel.Workbook book = null;
            //指定工作表名  
            Excel.Worksheet sheet = null;
            try
            {
                initailExcel();

                //指定存在的excel，参数为路径名  
                book = this._Excel.Application.Workbooks.Open(filePath, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                //获取当前需要操作的sheet  
                sheet = (Excel.Worksheet)book.Sheets["Sheet1"];

                List<Range> titleList = new List<Range>();

                //获取Worksheet对象
                Excel.Worksheet xlsWorkSheet = (Worksheet)book.Worksheets["Sheet1"];

                //获取已用的范围数据
                int rowsCount = xlsWorkSheet.UsedRange.Rows.Count;
                int colsCount = xlsWorkSheet.UsedRange.Columns.Count;

                Regex regSql = new Regex(@".*select.*from.*where.*");
                Regex regDate = new Regex(@"\d+/\d+/\d+");

                ESLogHelper Utility = new ESLogHelper();

                //专注列的sql语句，因为数组从0开始，所以多建立一个维度
                string[] SqlTable = new string[colsCount+1];
                //专注行的查询时间
                string[] timeTable = new string[rowsCount+1];

                for (int i = 0; i < colsCount+1; i++)
                {
                    SqlTable[i] = "";
                }

                for (int i = 0; i < rowsCount+1; i++)
                {
                    timeTable[i] = "";
                }

                //行
                for (int i = 1; i <= rowsCount; i++)
                {
                    //当前行的合计
                    int sum = 0;
                    bool cleaned =false;

                    //列
                    for (int j = 1; j <= colsCount; j++)
                    {
                        Range range = (Excel.Range)xlsWorkSheet.Cells[i, j];
                        string content = range.Text;
                        string sqlRegRst = regSql.Match(content).Value;
                        string dateRegRst = regDate.Match(content).Value;
                        
                        //对第一列（时间列做特殊处理）
                        if (j==1&(!cleaned))
                        {
                            if (content.Equals(""))
                            {
                                for (int k = 0; k < colsCount + 1; k++)
                                {
                                    SqlTable[k] = "";
                                }
                                cleaned = true;
                            }
                        }

                        //存SQL
                        if (!sqlRegRst.Equals(""))
                        {
                            cleaned = false;
                            //说明之前有sql值，这是个新的行，将此处以及之后的所有SqlTable值清空
                            if (!SqlTable[j].Equals(""))
                            {
                                for (int k = 0; k < colsCount + 1; k++)
                                {
                                    SqlTable[k] = "";
                                }
                            }
                            if (content.Contains("["))
                            {
                                int star = content.IndexOf("[");
                                int end = content.IndexOf("]");
                                string title = content.Substring(0,star);
                                string sql = content.Substring((star + 1), (end - star - 1)).Trim();
                                sheet.Cells[i, j] = title;
                                SqlTable[j] = sql;
                            }
                            else
                            {
                                Exception e = new Exception("SQL语句未用中括号【】包裹!");
                                throw e;
                            }
                        }
                        
                        //存日期
                        if (!dateRegRst.Equals(""))
                        {
                            string time = content.Trim();
                          
                            timeTable[i] = time;
                        }


                        //存小计，小计要在小计所属行向前寻找直到数据没有内容，且为合并单元格的情况,已经限制在第一行之后，所以不必管日期（以后需要拓展再说）,小计_统计项初始项开始列
                        if (content.Equals("小计"))
                        {
                            int curRow = i;
                            int curCol = j-1;
                            for (int k = curCol; k > 1; k--)
                            {
                                Range rangeItem = (Excel.Range)xlsWorkSheet.Cells[curRow, k];
                                if (rangeItem.MergeCells)
                                {
                                    string curTitle = rangeItem.Text;
                                    if (curTitle.Equals(""))
                                    {
                                        SqlTable[j] = "小计_"+(k+1);
                                    }
                                }

                            }
                        }

                        //存合计位置
                        if (content.Equals("合计"))
                        {
                            SqlTable[j] = "合计";
                        }


                        //表格具体数据内容
                        if (!(timeTable[i].Equals("")))
                        {
                            if (!(SqlTable[j].Equals("")))
                            {
                                if (SqlTable[j].Contains("小计"))
                                {
                                    string[] subSumArray = SqlTable[j].Split('_');
                                    int sbuStar = Int32.Parse(subSumArray[1]);
                                    int subSum = 0;
                                    int curRow = i;
                                    for (int k = sbuStar; k < j; k++)
                                    {
                                        Range rangeItem = (Excel.Range)xlsWorkSheet.Cells[curRow, k];
                                        string curSubSum = rangeItem.Text;
                                        subSum = subSum + Int32.Parse(curSubSum);
                                    }
                                    sheet.Cells[i, j] = subSum;
                                }
                                else if (SqlTable[j].Equals("合计")&&sum>0)
                                {
                                    sheet.Cells[i, j] = sum;
                                }
                                else if (SqlTable[j].Equals("合计")&&sum<=0)//如果这行没有统计的值，但是却有合计这个列，直接跳过
                                {
                                     continue;
                                }
                                else
                                {
                                    int result = Utility.GetCount(SqlTable[j], timeTable[i]);
                                    if (result<0)
                                    {
                                        result = 0;
                                    }
                                    string resultText = string.Empty;
                                    if (result==0)
                                    {
                                        resultText = "";
                                    }
                                    else
                                    {
                                        resultText = result.ToString();
                                    }

                                    sheet.Cells[i, j] = resultText;
                                    range.NumberFormatLocal = "@";
                                    //存合计的数据
                                    sum = sum + result;

                                    //更新数据后即可结束循环
                                    continue;
                                }
                            }
                        }

                    }

                }


                for (int i = 1; i <= rowsCount; i++)
                {
                    //列
                    for (int j = 1; j <= colsCount; j++)
                    {
                        Range range = (Excel.Range)xlsWorkSheet.Cells[i, j];
 
                        string content = range.Text;

                        if (content.Contains("%"))
                        {
                            string temp = content.Replace("%", "");
                            float f = float.Parse(temp);
                            if (f<=0)
                            {
                              range.NumberFormatLocal = "@";
                              sheet.Cells[i, j] = "";
                            }
                        }

                        if (content.Equals("0") || content.Contains("#"))
                        {
                              range.NumberFormatLocal = "@";
                              sheet.Cells[i, j] = "";
                             
                        }
                    }
                }

                //文件处理，将模板去掉，新生成文件，文件名加时间戳
                int fileEnd = filePath.LastIndexOf(@"\");
                string filename = filePath.Substring(fileEnd+1);
                string RootPath = filePath.Substring(0, fileEnd);

                filename = filename.Replace("模板", "").Replace(".xlsx","");
                /*
                TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                string timeStamp =  Convert.ToInt64(ts.TotalSeconds).ToString();
                */

                DateTime date = DateTime.Now;
                string timeStamp = date.ToString("yyyyMMddHHmmss");

                filename = filename + timeStamp + ".xlsx";
                filePath = RootPath + "\\" + filename;

                //保存新的更新后的文件
                book.SaveAs(filePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally {
                book.Close();
                sheet = null;
                book = null;
                this._Excel.Quit();
                KillExcel(this._Excel); 
                this._Excel = null;
                GC.Collect();
            }
        }




        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowThreadProcessId(IntPtr hwnd, out int ID);

        // 结束 Excel 进程暂不用
        public static void KillExcel(Excel.Application excel)
        {
            IntPtr t = new IntPtr(excel.Hwnd);
            int k = 0;
            GetWindowThreadProcessId(t, out k);
            System.Diagnostics.Process p = System.Diagnostics.Process.GetProcessById(k);
            p.Kill();
        }

       
    }
}
