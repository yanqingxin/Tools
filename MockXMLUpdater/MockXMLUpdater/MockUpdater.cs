﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using Flight.Product.Search.Entity;
using Ctrip.SOA.Comm;
using SnappyDOTNET;
using Ctrip.Flight.Common.Base.Util;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Noemax.FastInfoset;
using System.Xml;
using Noemax.Xml;
using Noemax.GZip;

namespace MockXMLUpdater
{
    public partial class MockUpdater : ServiceBase
    {
        private System.Timers.Timer time;
        private static string ConnectStr = @"Server=10.2.33.162\TestAutoServer,8765;database=FlightBookingAutomation;UID=sa;password=Admin_001";
        public MockUpdater()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            time = new System.Timers.Timer(1000*3600*1);
            time.Enabled = true;
            time.Elapsed += this.MockRefresh;
            time.Start();
        }


        protected override void OnStop()
        {
        }

        /// <summary>
        /// 更新mock数据主函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MockRefresh(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("C:\\Log.txt", FileMode.Append);
            StreamWriter sw = new StreamWriter(fs, Encoding.Default);
            sw.WriteLine(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")+" 开始执行###############################################################################");
            sw.Close();
            fs.Close();
            //string SQLStr = "SELECT CaseID,MockResponseName,MockResponse FROM FlightBookingAutomation..MockResponse (NOLOCK) where ID= 2148";
            string SQLStr = "SELECT CaseID,MockResponseName,MockResponse FROM FlightBookingAutomation..MockResponse (NOLOCK) where MockResponseName in ('核心查询接口','政策查询接口','单推往返接口','TransportListSearch','TransportDetailSearch','FlightIntl.Aggregator.Search.SearchFlights')";
            //string SQLStr = "SELECT CaseID,MockResponseName,MockResponse FROM FlightBookingAutomation..MockResponse (NOLOCK) where MockResponseName in ('单推往返接口')";
            DataTable queryResult = ExecuteSql(SQLStr, ConnectStr);

            foreach (DataRow row in queryResult.Rows)
            {
                string caseID = row["CaseID"].ToString();
                string responseName = row["MockResponseName"].ToString();
                string mockResponse = row["MockResponse"].ToString();
                //mockResponse = @"H4sIAKgUclcA/+29CXwcZZYnKNmWbxsDBgOmIMrchWTnfZimCiklWbKsw0r5kOmiOpHSdmI5U5WSABemyzYYG2NjKzGHMRQYMBhzOA9T+ADb0FOzPd0wjKt/09XT1dPHoNTR272lndn97c7sznr/7/siIiMyI/LSQdqIQ5kZGREZ3/e963vv/977x6Ki4iLH18aeoH9pW3fQ17nU0+lbur7Dt2Fj99LN3q4uzwbv0qZgoL2nrXtprb+7w+31BNs2Ln3U+E2R7+6N3d2dS5cseeyxxxY/Zl4cCG5YYjIYjEvW1q9wt230bvaU+fxd3R5/m3f4/u1X8yubvV2dAX+Xt2VLp9fxzeR2U6fxbs0fb/d0e6Qn6MbJXfhN3GaOdIMar6fdG/zJjqemrvYGu3wBf6jIuNgwjAMz3YGeYJu3vLOztjI02Wg2GA0mOj7djfHgRBy97hHz5sfavY8Zg909D7d3t6//hdEQ2NDd9QidN7sl6PF3edq6+bnzTEajzWAzOXAn/GOyGIwWdrt6Pj21leyTa6PH7/d24II51Wz6aLYa/R0+v5e+n+XueVg+Ze+MoiL2pOItaDJCdyUuW1y+YUPQu8HTHQgu5tMmvvBTuujaGa6eru7AZm8Qvzile0tPt7erm45Pc3uDj+JoU2i60bDYtNhuWmw0ONgVzd5uTLUr0O6VHmBGpc+zobGnu7OnO2Rwb/QEve1NHZ7u9YHg5rvdq1yuKrd7qcOwxH7PfS4iDvmYzbDEaLLec9+l+7fPkte1p6P7/u3X8UcUSWZZMNDTucLXhS+uTv3i/u3z+EG3d8Nmr7+bnzhbdQyfG4O+DZUYndv789pKPHkxyAC/yi5RHpta21Xe3t7o3zvfgDOmidNZecCMT1PZk2CpQEFNQV+bd5XfhxVq9z5+gE6eXt0mniDen07a7AluEX+Gjl6i89i1/DmnsPf3b5+ZuF/lASJRnMT4BQtTZCqj21/VHMD68IlqCWzy+nc88IR6tkuNpYbS0lLDk0/UrzLbnHZ8bsW/pa51Tz5htNsNpRa7vdTlbmrZWr9qK85qaGx+kj13xxZvsLrDsyFU3BAApd2/fW6zty2wGXPT3lUT6PZ2iLPh8nRtrPC0bWIrT4xn7zTepc94XAIo+O7aikBgk8+/wdXh6eqq9a8P0CT85Kmnr0o+jmOzlMcOtA7j0AyX52Gfn10cmlLVFvAHNm+h41NcgR5/Nx5qJk3vPMzc5gDjO/EXsPyqY/dvvxbc+ai3wxt0ebq9GwLBLUTPoUnl7SA/IoxlvvVYWP58oALp4/3bS9hbENC8d4k8avEQDZ7N3lDJY771vsWd/g10dGWPx9/t697Cpgmf3RsDjxF3MgIQZgtzcPkXw8Lc0PR273oPfpJdKFzFzhfm4WUSDWSWNN30JMIk+vbSJcc3Tz1N9wSHsfHRTNF7aSChqVWPt3X0dPkehcR46unZ7kCQSNTXRnJg79ziouLrn6YvSpoCPn93Fx2ZPOcBNrtVj3f6gt4W32bvK1OnCde/ucd4YVZR+CrpJ6e2eDaI6zUJb3HFpDrvltAM5a+z1Vjt6ejxhiezIayVyJiGsBDP3Qx+80DAYTSLccIMzmA0/b0GonuIuM7ODh9WBofweSZJIrAIk25FjW5ivpLyzeKC0/TO4rdQH1vbydlHnPYpbDpxRzzOoz6IfDaZ9Ptu32ZfhyeYWK35q/yeNhB/p8fv87aXb/A2e/wbvKFiQ5nRyri3tqvJE+z2eUSmmFHbRZTqebjDu3e+49K8q6+5lrH/fPb3OuKc63uN1uEFwzccH76RHbyJS4KFN//ggGn4llCxkRj81tD9adjZajRK3AxeNunwslD00bBQTD/JyQU8CmoRJgtThJKPh4WpJ4aFaZxNhOnCDGFmGOQ2SyTId0GQ4WIVFaqINTwpmUKFq1VEKVwjXBuePCzMl6hMuE6iLuH68BRORsIC4QbhxnDJsHATUYiwEP/ezGhB+EF46rBwy7Bwa3jasCBwXvghf1mElynDwm181YTb+dE7wtPx4U423LvSzL1de+7fZ1Nvoanf8ZM0U+8wOwzS3NdVYfYNVk1BOjH5qYR/wKFD9xZMfpGNzf2P05G9waKke6NRW4dNTL2WzLHozL2VEb4xi8m3KSffMiF0chA6znRCx56F0LE6JoROfhL/Ax26t5HMMWRB9mazQtdOyJxcdK05HdnzuU+va+2jSfZkYXxvDJ0DOnZOeCHRvZXNfbodo9NhkneM1fWwcyYEfvaUH56nTfjhq2jyzdlMvix02ORPiJ3sJ7/XaNQR+U6afUfG2YezxJmVs2TC0NQyNHWM/F6jgVma2Zj51ontbX7GTnimvsYt4jZ+eoVrtE3YmflNfa9Nx9oJ38Lo3llmZLPPvN4J32x1vcPEZX1DaSm5E5zcn1DTumz51up6yTH7hNFqNNmMTpvzyWESO6Ljb2aFZ8MGBAxET9xU8SP8bi8w3xLewMcGr/Om2srwHPp4PEw+Ex2v0IGGhFsILqlUa4n8lGlcP3AVw12udrbOVBy5f3sx8/Ud584j8rrDgVbJ3Z7w2ZCHr8IH/6R/A7nqProkLBaW4MRPpPMFA/PvGOVvjqV+c0n0c8EBFZrk6gj0tIteqEl/+/hwJi/U3WqH0s0/6DVyp5xTZJ6My2djy+euWd6aafmEu4V7hB+xeRDuxeoIpenWJnzXyNZGKBMnU558mkyHcjLznuYZiFcE4DX1/QIrJ83279+h2Wa+uevhQi4qfvOmYoULWen7uzdnN57NpGNh3chWiyv5ilRms0ouJHBb/SrGb+OxYJo+1gzMNIYLFr5Xa5HCpale2ZxXBiFThEY13dthtjZWroY010bcao/v2iC6krOguzzXpteqtzLXDN8Sns92hEnyrX6VkmMUOxEm4BKBQ+gni9ngMFnMCv2Uv4Ab7ZjEuKzX7z5LCDw9XhpphOPmH4RniA5bMieyX6+KmtbKifWioNTdkuz75l+yMgdGumRZBKUsOnumMPGloLfOCWk5bnypKStHECu80vgS3gVdttRcrrFly8tpuQqQLa06PtReI4IH4blaVkz9KjhO5f2cyne3zF0/hhpTc7tW6Jz5z1vHXmNmIX7ter7yOaRqzUbtnXvjOrONhyfq2M7daLeUWrCbcFe0urY2rpN37iaTxWk120x2XcvomgxbvwN1GdAa/yZ/tIYsgUErfE+esi1UbRhVu2+NDaNi9y1r2skzAfXIAP9I2XiHfyhOPndaJdmlqZNvZ5NfVwO3yahOfth5eU6+tMWbPJt5Pfg+fFS2eL02PXOF8YvJrM8vpnHhl8t1ySQVOAJ+weTr8ot68if45aNLSmE1hvxi19t2c36x6/OLZYJfZJ3ENc8o6xdMvi6/qCd/gl/Gi1/gQkSsUNuFyDjGqhNLIaOAx6/G2iL7HmsYTL4ux6gnf4JjxpFjDGn3MFabtl+COMZknWCZsVUymH0Ot9LYxCTN/gTPjBfP9Dp0YELh69jGk0PkUjaernVmI/fvrGC7fmQn8oAv7fpdil0/NvwOs9U4gl3/igwbz9+Mwq5/9IPBcmxx8ows4iGK5A3E2k0MIoSp14RK6E0986GO6tSHay/PqZc3kFMqc3W4ZOEms+ipGGKYIh1DGotm42i6seaXy3XR5I1nzvwSXs4nXo9bNCd+glso8UwZCJw8Jtxi18NBmoxMyLEsvx3lGhAVMbkP/MIQKiarOR2kyGrJP+KeScwVAEJl8m3CaLsv7XpAvSbmjkmDHRJxqRMrw1I5R39lnHoBNxMD4VlsZSYtm6yuygHXP+mYKm6TWZ2lFoutdA0FA+qqEpEYK9CVMMpM+UdiqhKGwYwCww3Jyn8BkmNzjbb0msziDFvSz3A9zTBEEjYqmGF3a3WLOMPuqnJ84bBYTGazRV8mZYx11Y9shkUIm1K+l6zvyGdCKCmVSM6czYTYkJlKE8KCvJzkRmlCwutGbUIkS6PkpdxCQmSZszRRzIamrZHCgE6EQok8aFM0qgwYXjOy2RgPwMnkOx8YbZVl0/OHrhJzKnTlIndij7VcLORlGZFcDK8VJ1hXLPIJHmuxOIpSQJqPvMRi+EFxPnSlonI+rnCpGP6pOBm6QlHFfRNCcZRhCHr+CCYUTSZ9Y5HDdsZTKGrmHn+H26tREIqYYF2hyCd4PIViPhM8WrYiF4qYD12hqJyP8RGKI5yP/E1FLhQxGbpCUcV94yIU85mMy9RStOiUGgmTUCxyMpmYEoiqq0IpwvHZQV/ZliImmGRiugkeT6GYj4tiVIUi5oNkYub5GB+hOML5GKFQxGRoJgekct+4CMV8JuPyFIqUk6gT7O01UR2gYqtUjCY1/S3hjVeU2kvOfjOZ7U6HxejUj/ZmTO/N5IsvcCz/5FuRSpzJvh9pKhU5ouziemmaF0hX1Fyv5DSb7+96JQz/jVk5RUe6ZFnEle16QIwAsyR1wDOUmZqIx0zwplgXQRPSPD68Gf65uF66KXCa6zXBmykB6QWFwpukOXWipb0mB5PEPI6dks7vWmexikCpVaWlrFKqRYJKrWvRgkqZbfmH5VZdnngdGfrxy5OZlWcSVIqKWdHsE7PlMvt1LU1rNNBSI5j98JbLc/ZlVfjEV1mpwtwKDTv0kj4eY/APrtVS4B+udQquYfCP/Lkmo8l5ma5b/lwT/oU498Q0Ocx97jxzpc79GPOMTpHiXjMHhvLses0aWrxYJc/7sFg4Jreiec1ajSJMVmUNrRxrlGRKBi0El/7jNbmKM+yrzByVxsFPmWcYllyaMldX6AzLgmfr/52Tus5iD+TQs7LMDPlk5uhazaJJYllukD7XF+OyNFRrP6WW7ndI/GO4NE6dbg29Zg6Z4j0DNCvxJJZGUUBXp3JSGgMsozJRSCXNhSl039GWv8nMTyN1RJCM45Aui36NFs0FW7ZupVbhju/hgsna3zc7KxUz0jXLLDhpg2rTzhXtNTPMmpUn8qRYe6xSi2w1qEpcu2vKNUpl5bbey5XdVjKmi+bNoMsvjYtL/sWDY8+gmRe715miJYsuKRrsTBIb7BBR6ADmes3M32/hoZj6ZPT96garWL1nZWlp2eoGi4EHq/EJEtwKSKMd/mUG8lzdkMAVm80mh8Fs0McV+zJVeFmZ2EJPu0QNesIvKI8UlrKVxMCUm/48KzGg9lyYmQcfK0BsmfMKONgKLK+GE2NUV0A934W+ApK5M2XhgcyMmaPfIsXc0eSxXoeu2GV+QbM+h5l5dW+Rw0TcwQg5rNeC8hDp6+eqGKrQF3iELMacg1gCPRZLuwT5stirV9QKjB2LIW0vqbiFJoeFm3RqYLzKUKfcF7Jct11WXblDXGWkK5W51lqMXJXxNEwrGgo5DaUrqmvqtrrWyu0f03U0UOZXzuA6ShGz1kQTfKmVoTwsd4obS9NFaj036Vu2txAbG+q3nstJSIZvzW4FF+mvYLgh0+oZ5DRztnocnjWxepqNA3NbvbXZrd6CdPxn4knqafnPyRWdxH+8/tbECo7CCl478hUs4m6AtAtokDPXGQtOLCCVENTp3ZkbC2a5gHt0tvYW5q228ehnXTodmLSEyVLU6DR9D5Tg5Ou6R18J0i5bB2zca2FeayuPJqS4XuqqrA5xY+0W+1HbrCaeMQjXizJh0AbkntFsyamKilvpejngHknCoHt83CvfsH101iU64cq0MN8zJpjgxilhAcUEA25MDb9NNieqCLAJXludW4JqRu/z6CWoyqlpYdY7JK3RltqpxcLcu5gTghxnMyd2Eye63JN2x3FOZNDx57tzIpLM7jRiYL1Krm+zqeRRJ03+FRuSgn9Z1Clv/tWYSRX/ho9eifwbflecX132FecX7Evz+z1g3/AxcUp0uTdpSia416ATmQxz7uWBSU3uReds2ouMufa9ornXoq98xfn9/ihfkXst+ro3aUq+59zb69Rzn1t4pQ2dqCXwgeKORsJ72NOjao3G/MtDZQI7FXgBwinz/igni4nMah4d5L4dLYCgzuTrAAS/h5MvBy7mbMvVhM9ssfY69ezVFWzddFBS9atQg5CH+mvE/abdziGCek1ZrHlzzQGg76aeGBamYT93ObYxm3LPm5m5ZnyAHTpo0DBbbCvvdJCyz2OLzf12Y7/Y4dMTi/1D7rhYhBcUHruNPuXazjMcy86f+4mORx7OkPDVWmiChrVGMy9vvQZBsIa1DhN3AOITNq0Om6EU0I3SmrrW8q0NchTsCZPB4bA4DHb9Xob3ZMJzKMpUzeSxMhTDlcTClEIFT/7r/Mysn5Oft9eJ3D7w6w3Hh29kNbZvYuShgdnpdSJfIvlMNLVaiI6HPibcubN3RTJkB3FOMc4Ca1vhpmd59kaH3UZxzjXr0K2yrjzRTdlpMdlsZoOBi3m9fsjKqkvFfBkVzi5Vy62rhUlsaGMZ05Thd8F8CtpZGYYe00hiM8dpdCL2gWlksJvLfhplrPB2dI/Owe2ahX3igEO812bLit6BUAv/mfpMTu+9VhbdsPC+YykIKRC8aIVKBM9lWoLgKagxChQvAtEUFD9NKbjGleLfZiX1cnMIh6Vp1ETBpJ9GIniaxlGg+AKYRpniY6wueNaBhmwoHkW4wjOzIXgHbPfkMyWC50h6AyP4tckS3lVuNolgiNWlpTWNZS5wAA+t4zOEvBNa3Qosr3vdypqtLoWUt6McqcFUajLZ7HabyaZflfTtTPp8tdLOY4oAOYNpzfzvMBlC3oP15FMJ1spR8rwixprcF8Ph4IuBSN5orUW7CkytmvmRr0X7mOpsGUbWe/sos174wvCCD7JgvPBvUs6T2I65moyc7R5MXWmLzcYhD62lpW4X2M5iN8l853aNCuchczstbPPj7xHjUVfZm/JcCpMBCDHOd63V+THeFbUUMt/9avoo812vlRTZvCw4r9cKczD5THFP8w1jPe5o1NB4KaxnFNtHj5bGu6IWe0QKL8ylIHe4a+i7TEsxUn2nsRIqfTfKEvByVXe9NrgDwwuzYTsb4qLJZyrZzqRraGKtOUpa0ngTbJdFa7Qp+diZnO2wFPpsl3YpJthONIqp1cEYWplvZueafVfHNftbrDKgtpo+J1etFbszYrcWct3VGkVzE58AeTEA72FGT3W2kahVJNtZjU6L0ykWcVC47jaocH8tStwQ2yX8PnGkJGcP7Ibx2SX8LK9dguObye3WTuPTw/dvn98S9Pi7fN2rfV2eZu9mT3DTCl9X90+efnpeyhc4+ALjbry5JuXb2srQjJbVZUZnKf46rDbM2F175zsuzbta4b0tFjMuEf7PKqXlVX0qCd+s5ZYkEuFF1EefRJQEISa0KEgk9/KYBU4iT6PtlUkwCxYu0a3hv0u/pH+R3ZJ+rbOkf8WsKl6EvSF5b9myzGZLxGXwScR2sbiMyWzEkhsc5tLWSnfF1pZlCd43GBxWA6CsutFZBJ3TZwEqwjDimquOFGai7VTzzXm4P38mrgBtMXJcAURH+AqgWEXl93kFJMU6dUMWJSpyC43Z9Mpe/gPcANdrhbappp7o+kdom2rqmay66B+LHZgHpyPvmnoZQ9sFjv4p2XdDTht/xBnLGMPwfoqaJfX0Jt9VVaEuqfd9nXwZwP/cv+QqsDLHGsLfYFt3SxYbwPDF1BPF/d8dbInN2qFk11qpVynYSxFKxidiNRNLmWWBtUTKLKIPEJYGJNRmCCUrkCKi4lEd0VA8mtmzwmxhzt4Z8wohuvBye65rDB77a3EBtDcE6RcA/UyxAAyy9X1eAHmzdyyLqiw56STEo5PMeJFtfsf26kZ9QHkiUMDTFRy2NOlcppzKpO9SbusUzrDcDfRd47KHK/lo9DuW6oCFw2xdzNyHogn0xzqI7qyJdRmDddFrtsLXhSMUtdbFCbNNXBeqPDXBL6MLSrDrtUvkckwHW09Zkw55XSb4ZfT5xabHL3/G1iVNwpKNnEAu/MvS4cxWRMN004VNkm+QFxq4W7hH+BH3edwb/mGqfyCichu64CQMlVT0dPn83q4uDuPOHa8ZuaRooyPp62km1CgaZUJPgUhrgyvtepli7WzieaGglNjXuhqnCRgoElTNMIjx0e7gMHqER558wmKiOrvovLx2WXnl1nU1ij7LJpsRxWdMNuxPjWanPu6mNxPuplnpsyUvrioaVljeGmmdp+/9MKd1hlV8n7gM5CzLeRksJgOL/Fcsb147NssQ/t9HeRk0mkxN/68sWydHiN8/ixNnzWXiXKXLGf2SFMli4qz6TpRXMrkbUybuwHJMZfhfuWCZmnMYokgpWKTd9oxrsyvHmdNeoNehp0QHmXHDhbV25wEOEoKQIB8VfFY818ad1PfBiGCr0Wpy6ufaZGoWrhAGmi3+CtxFNaMmt+p9EBRd4tzrd33QmfskF9X3du5lnql+LVdxo/JQwWiY3G7mMbfZTUFfm3eV39fNYm07n54uH8CH1z8Yxt9Z1Z6gt9a/PiCeMk36jO/EKNzOp0vYwUp2wTR320Zve0+HNzTFaDJbrDY7HZ1OZ1R4unxdl+gj/2XFbfkv0wF8e2EuAnZ02oyqxzuDMGd8AX/odsRTy+pXlbWWuatW3N3QWF7aZDOYjGZYTfbShkZ3qQu/5nDeQ9cdDxfRS0n5Bq+/bQs9FH9XW7l3RsmD7M78gCvQ7g0Vu9xNLex8l6+bnVM0iY2EPrIzivDbiqsaPJu9oVv6zh2OnzkYf/GZ+DM7h5469ofPdscPfz50aGf8dCy+P/yHz55l09e4fr2vzefpoGt2eIRx+OcSfnaOK7B5s49NXLOn27t7UtGbF37G1sHt6fB2sWPFN7NJmlb1eNtGj3+Dd3fRrWyMTcHAel93y5ZOLwNc0yBafG2bvN08LEsHrmkM+jb4/J4OtmqV3m6Pr4NNWE8wSNMaKmqur5B/jp20u3hBJ7tXU8/DHb6ujeLBeVexg+VtbYEef7d0kD3qpBbP47uLl/2S0Ut1j7fD3RN0bfQE6UnZVXPd3u7uDu9mr3Rh0VU09mlsyr0NgdBkLJvdyOnP5QkGtygHNYdmohIT0UyD7wpdAyQiYG3I1rH+mECJ9BbNBXDtfD58n39DpdfT3gEDW6TTeSlfvDJ1mnD9m4/c80XdlJO/3FGy2GlzoOwre4IF9AQ+bzDlmkhR4kR6/k/x/yn8f7rX3UQXnglNKeX/XBJ+JvxJqAjkOix4QgsNiw34p9QIY5W/oxf2h47yk4sbvN3s7ClYEYTGrPzwvGavp6PZ2+UNPurpBpHQxOCsD/i3sxn10HF3oCfYRt8c599cJT99i2+zd4UPZ9HNp5m2WoB134p/+XmzVnV5+alsSdmtaSxXN7Cf83SAs8o7OgKPedvZDCsOVwIAkPhmDvgUz9mNu9CFdPI8N54aN632et09G7B0JLmubezpfhgk1N7sfRR3b2c/Eyqu9Pq3DP8SBFvrT/n2ie07Jvl9Heic2ONlPJO4xXrcKDKbLpwtX0jHirZx6VXbhWFh5rwk1faigxkNLXVq6FR5nkTaY5JlOps5+gou8X9koqe2axUoW+K4afSBf4/cLLoPpkXBIpwBrmZdH5O/YY94YTr9mSHcx4ESeD/zg2HBFUaKI9FiRSCwCfTs6vB0dUlSmAQ8Y+56zyOBoMvzsM/PvhcF/7sn6DlmVPnB+F4aTGiWG9zobW/q8HSvDwQ3s5mRFUg9nhcHpoJj/X5vR2jSutayNW52DicqrjK6QlNlKU5fJghMZJfe+lV0/Fp5dsXjTAbP6Dv1Wv9Lpwd3Hx94/4y4MuXt7Y3+5QE8ZLu4MjP4teyZi8EbxmGhkQ0U2JFHvR3eYFUHpNnDPiLKUFFDYzN9OafZ2wYu8PrbV3hxkqQT5tX7/E2YFC9kRtBFMkua4Hn1nsdTvplBt5qKa9zdni38vedx6f3VEsElpBDNgMgX8kEmmdB2CfE9A0kmJItby9D+kwigOhB82Nfe7vXLZ9MizROJNnFfYdewsJu+msvkf+KLMGfL2q5ajLXdB5IWSUPUwHPFj5Lko5PL2x9ViI7qYGBzr6GSLZ76m5YA9YuyInXnOXEl5lZ4QSlQGJ2eYHdP0NtrFK9b3+0NKu4ZuZ6e9ararsZOr3+55zFxSqT1rO2qUx9hRgpXT7WVyNXBxbP4R05loSlNDovZjKROIuGmVXSUk0N17YoqTjj+R6FHAlxHhIqqy5E588dK0Tws/FT1kdNIV09Ht8sDi6fOuyX0A3OFs6K8utpoqKiw2CtcBgvwXhWGinIDytYaq9FZS3iIC1isfAWTVoEe0itMhor2DawbUQorZIwonxECLDWJInZOMkfgJpF5/NK5KVSaEO4lTYEOX9uWhgD96Dz6Ufxi/2vb+j5/fvD8gb6zn4oSXD02uvlCRitz1FxP0qy2q8ELOeAPSkRT6++GaRBo72njXDeluaXO7fUw8TKrvKc7IKloOjATUx+APJeVs/DwsNA2LDDNAFFb3tmJB2YCHQRAhDGztmvZavH+Ik3gkGudu9NLphYdEl47UI1Nql1wgBwEp7BUlITCH0EM3h9GH5wf4+ufCA8I5fTQQkX4vzPxOCxUClXMSBSqw/9jWFjGGF+oCf8/w0Jt+P8dFpaPkxEn1IlGm7CCWWpCPdlnQgNPgmscFpqEleH/OSw07y6+pnNYcO8uhikltODlZ8PCKmY3CavJUBLW4O8lYW34/xsWWvnV68D1woNpyfsSp9QInI+eyCRGEhGUJvZEpvD3JfR+Kn8/TSauCHIWRFMhMoPOQA45nTGLGwDywgpeYT30659sgEIVNtI7H6lW4RG2dJsELm6FzUw7Cn7+1AGep9zJBvVzGtSwEBS6pIXtlvSb0DMsPCo8duKS8HgEVdq2CL+IzB0Wnojgqq2RecPCk5Grh4U/Zb/0ywh26pjLbZFrh4Xt/Ed3cJfrU3jBEOCH3TksPCPsiiDFfPcl4VkuSYU9NIUQayCdvcK+yHXDwvOR60VBJ+yPLBgWDkCICb3sZ0Ls7wtEiQcjQHe8GLlxWHgpclMGCSO8HFkoSowIcFSeyA/4bN5C72/l7xGyk7g+Ah+xaMFFFtHh2/gpQGYy3hVeEQ6xB341csewcBgswudV4jbhdWIv4VfsYd/gTBS5kzPR+1c6DxHXFCwPTTDOZcs4yKK6wrXP1RhhwXLOhPa5ErRPGNr5yjbh5k+YcLC5Jky4MTThwrBJr2wmmtBEtHGZYKKxZCJsyK5sJprQRBNMRF6DsWQieGKubCaa0EQTTDTWTARn8JXNRMALTDgWJsy5MdVE8JNf2Uw0ERua0ERjrYkQj1IyUXifFGENTW5wN69x1akjraEfUdC5ur6sQUbyNRitRkDvnTZnS2kTeqyhSorB4ih1lTmB51OGZYElqA4V17QuAxpZFZoNzeh/aVd836uA4/W/jIgd+svX7S6ePXWYhU8RrVSHT0P39J3aF99/Ir7nzcGP34q/fzL+9PmBo8fuQxy8/9CrJks8vL//5V/Hd52hgw8pg63zXmRaiSw8CrZWaAdbAT1bt9poRLBNCrlKKA7kIhqV+DKKw3LU2C8WfHnvA2HYxj+VP9/XGkboNhGJDc22lVJeA1Be/N9EYDY0tbTUyvBfHCbGIrShEo76spg0I7Ui3GsuYuyIyW9GiL2NgwooYngnv4TCuKHpxq1GVCTdihcDFhPR29Ck8nYCf0khXKsihMugm9OavAD5+LvXotyjGNGFcwIR3RKU6aeILj5RRBcRYlVENxG8DU1i8CYWwQ1NXlbpJvyQ8ERoqkwsCOj2VtcjoktAoD39n3wqAoGU0V05tItIp05oV4T+AVVjNGNpnOgADugfwp883hu6xoi0NywbWzkj3ia+3iNdi67GBM5JXKsZGwY97O+1g4hTgsMHQ9MSTIAIcagIA5aDxIsJfLJkODkgzDEj4CQZM3KNCpnWEmBgS2ApPKFJ1Z6OLm8igMygJDZFENnJ3yf2faGZHOu12hskHCluEhbD+XJEWRUrDv9eBFyAZbXlQeSuHCWB02ky2g3IoUqRBNNAVZAEKPPRmiwJFnJaiD9zeODwHg1UbibZcEDN7y+p+B1pVpnAFUkkQdAtGyMnFaevYpweWZLM3gSD12DvCMdzqrnanIarI+ZkJp5q3AqIpoKFp6wIPOYKAPSnxcR43jePqzm3CBCIrDg3YuOwCzvBLkDzWyPoyPNkZKkKdpEFY0buk3gwghwbQl1E7tdBXRBnRX6sxViRnxDgAmW4RcBFnTYvRcr5ZEKgeyKuJD6xp+OTSFX2rAHcx6iyhgPJhclKkrFGZFkSW0TQ3iWDUoygLXoCVTQvZ8KPQCkTqojPsApBxCFtHEGkoORIfS4UHGnguifSOEGxNJE6FBtpGi1qBThKRa2gXg6d0xbkSM5QmXQWs8FhspjJpCNBbnOiEAmo1a4y6dJQa2iJUpJz3Q57LL7nSP9r+9jfZ4cOHYjv/DC+/xRqPpmQXWtRmH0El1NbfWoCF510kiVnzVKyQ5wnbLiEZNegerLWqKGPlrU2uZQdThXqZo7QFyFzWvg6brWlyPdx4g4yv8SF1cHT5SXYFUrTmZ0dpS3tmRklP182cl8E1SXLfRUXcVit0hORg8yHBT1KXGQ1on+Aw25N5aISIFNhDlXUtMK4VG+MZg+ce35oG+UpYWM08Dkyl5eHlsT3vtx35sP4vufZl6cGz53j7KXNWDazxFjAd65gONSk7RQ4NfvtVGvrktVNKl2TxIrwymgZWaHJTVV1diDf1ZsqVEWA9U1UIybtqE2tC/cWaZpaDrCftqm1OpUrTeIGigNZc+FKi6izsEES0a7qrZLwZt4bJJWZtXY0uTECo1p3A6NjZqHdipLdgCrV2rJIGNYUdqtMi2fNgd1AHqPEbk4L2m5Bg2mwG4xrsBtVdErxQ8B1AKYCrw0e2Uu8tix++nz801/hWN+ZVwcOfhL/6ADlDO45wrMF+YkD75xTarP+TyPx12Lx3c/Iys2IdGOY+3UM/KzJgtMe6n/2XP/B8wMfwXBLmHFJrIXttTZrucobMNpUfwW4S/RXIAElaRfzxRMGLdaag+5kVCRTaxujwVv2Atd4kUJjLTTRyom1lPbgiDQZ0PQq1oInh9uDoUl1NY0pfj6qgFzWuK6sLuHnM5ksoGObyd5ERqEFyZEmq5kyd+H/U3v6UECD9vcVrdhxJyk00jXvvBZ/43lwEHf2SZai8hs93oKlaLDbzXB9kIMQO5IVSLS4eQrw30k6zZ+LTlvZUrV0SeM6AUNGAej+3S/hDcY2tHPfwOcfcz+iEN/5ad/nLwye+DS+f8fgtqf6T8DHeHTg1af7n3174Pldg0fQ1UvBt3DCkZ8R/i/4Ga++jfFtLZJ2VEkdLMXVZIXtmuxnRJmDNH5G+BVTvQ9zzMgl1bFXbxXTSuGZpX94jqllMZVIUCWcKh0USP8uRFUZmipTITkQG6G44EAceuN9yOcROBBZhp5BNENgkZiQZsgdiJLzQnIBi0uTha9Q18ZVjECpdB9L5ycEJ0p+QublCBW3UOZrIqVEqYK3jsTihQ9RW05EAkkOwEwiwmF0mlGCw6YpImAUQETU1aQGA4pcLpRRXx4y9UfeGHxuf/zMp4PnntXUt4Mn3+47e1ZWszCvjTD1KPdq9uT9urIhaSOJaVew6p3pWNWQllVTHYVfPKdpvc41L9a3X7ek2K8IruTEiZKjRTZaQyU1vg0bubNQmao1Oi7CJ8h23UouQtQiVWRmZbOT/NMUFyES33K1XVF9W8lGz+oo2O0KF+EOBeMoFeyIGAdpZCrGQWRNVLAlK0x1zU1rVmrG0lzrylYodCzUqwMK1U6OF7vVZLIbTE4NG5Y0bOTnyW7CIPgm0sWVI0JImqZmyUNM3bkgORVZiAiwgAuuwQspLDhctQzNCAzQZFWFuIud794U7hQuytTecWsOWzajRQyO5b5lS6H+iHLLNlqOcaghaaFIDdFkQg3F972cSGjPkRcSieKyye6gEhaiGtrxN1IeuerrJfwogl4WcRHw1sqPosWaQTzK3iaOOsTtNoJhBjoK1WcsM7Gj/K1TPIq2rWxxcRSlNdgd8JbOEY/SI4pHpWcgPSo+AyW9i88gvuXn0lHx16gPHA/W8WgcBiNSVL4a9oUwfDYUkVOsUNZqFsyoUrPKYILKqXRwJCoWrnZtSRHZpxVjSyMh4FRCER27loQgp1Lkj5MlxE9JQjyU3tEKa1pXMqCSkaZk+JN0kkEMuVJ4gXsWkuNm2TtzjCZHvi7W8XLmvEAKEUWSn4wAbZCjQsTun5ud3wN+j7ycq67nrB1B/btsnFWHtGOCEaWlPCI2RmkeFRvHEtCZFe5qfeiMQt2LaAirRYqz2A1jGBW8BqAE0vLithSEmikcnntUUEStwPQfmdE6RmpbnvDvOLCdQIyg8+1IQtyjFzBES0AVOcNhJNqvk6tqGte4yLJS1NwIse1fXVVZlcJ6RacQK8xXEydnNC8wOqCbTGa7w6byEE0FJWL7t0Zj+zdDdLme+RD7edoHlvID8VM7BtixgbP7+z95Pr6/Nx55MSmKaLCbDNIOsARbCU34WAQlUxX6DU5g4gkURSTL9xDjicpUVw1crEY7MBGp9q/BlGL/ciefSstdZV2MKrbUXE4roiiXA4NPhrtnOBxMWQ5M5Z1xioifpPIdeuHFcdJ9MIslCiCzuA6bfDKL33kPDpoRe2dEq5XFi/S8M3B402rkbzvCNGO2Y2IYSlWj47zlUC6wgr7tqFQ6H4/EdoQ/W8WlcNdIXFrvblzjdulwKcOZscqLDSY0KDchKkhKBx5dk91itWpzKbbW5MdtrYZDJhmx+coLQ2//Kv7O+32nnuPeGh72OLWjX/FNmsAkirA6EDygrSqIWM+P68jFjzt09NeD5w9/u20H/0/J5neAdcHmd3Lk5+136bG5e91aoNZS2RzbKVsy8jOitc2lUtXaPP5DI+pSc4bO3g9rsphE/FLBcbpERaRJURP3ycgJtbmLwptV5bwyTwb4JvMdsPlNhm/K0DG0nsnNL/RCGOYZY+XEc2axDYzEsrAaR8TAKE6lzcCRk7mxrtVEtGQ1m7QUbAkFOSPrk7d/KFa2PLIxU/Sk9yFsyRMq8k6sYoJ37tDhHWWJqshZDibT0IOzUf1aJ7z4ebLrE92PciP+8XH+RM7rkXwE6L309B4BIasDC3QkH9KOoFNTFrpJl6CVfs8RETRcCtp2Y+TtHC1Gh9NuMJsIMZxqMZZk8njC6U9xQG2TTwVYuQab84TJdw0K+2mZfEpnZwQltIieNQT+XMTRdD2bojmmQPObzOnQ/Pq4sAQCBfjJ0Xfif6RL0WJtNX0JHgmnUHQkd4omuysSVVO0TkBs7Cka1r+KohHbFaGTv9LyzwE6qfTgm9EA0WJ0Mg9+XtDJiDsHIKSYIybt6RFQz7SnT0QyWcaE2nOvAf9dpYD/piJAjGTEjQzzqI2uGkVXvbQi+aOskuZM10z597lSPrNQEs9XEJhHhJVGifxFzCMslGTkMMM8pndP68AVI2r3NM8zlsjfl7172sIThlSwQw33dKQtPfnbxI147pGqcdqTR77On+wjoOckayV3Co98k52/Vg/LO2rgQugTFV2DWKQA7ar0AdpVKQFas40lO9qRnWe2GlLpewaABLR1RveR5K3zAgI1wJ91YO/AsefuVrYdgItseege/gXAvgNnXxt4r7d/z7bBHUf6zn6e5OuCZ83J9s8wfIre0MUVDu46Hv9s272PAJGUsOjn83AvvWA3fO0z6fIgTbCP9IO+vM6+IjvqOh1c4VzDYqde+mMkVa2YHSLoIXe+Gp9NgBwBNgN3ORHvySK+m2+8JxHKpalWasjf60A99CI/yi3PiCI/v9CTJJG/0gng6kkQu81usVnQYzZVQ6JdEyGkWprWJEuQqQaDXc6VDt1D+ZL7XgZOSs4U0M4EQL8VlmKji0Kuz8XZFj9xuv/EQYFn+aCU/8AHzwkCsJOD23aSzIE38DO4D3VlzuN6MqemvA59ghIyJwLPvI4vAVhlkw7mUUuk2As+dPz3E6JEwoeIYBNtqEi+oiTyD9n5TMY8dAzOUJki8BtJXvy6dKFjBRpbimSyqgsWm91gR5JeakLp1L/kyUXNaxA5UPvwf8gTifpOvTTw4l4AKft39yKbQUgxSRR4Mq1qDLtykRrdHn//wdP9r70RD+0d2raNMgI/Pdr3+Wso2DD08sk/fPZqvPdY36ltg7ti9ObMQScOUdK+fanB8IfP9tLxcG/8tWMUWNj/0eAnnyIIYUYUon//gb4z76jkDUBsZOPwYPe12NRo505ULHdjw66QN4h16Ph65liRdZ91irfZLibLFprnXqacwomBI6CXjXNeL2lWuUVIKYWQS4o3YkbafBkZ1Kl+oMeRGaofjDzFe/5KFX1vz+z4yRvMYXbkRsnjY4FHhr9j+o2gz2Y2DpsxT/WG029UqXYMIUjjQbWR/1aA0KMJapUqzOxOotaLsnddU8bCu66SsSw/nLtfxt67LhbUlYwItObO5F2PiOZDzo50s11EzBWux0NMzuceD41mPNmkfKRWhck9XTlRJyDZI6BXH2bM6wTsGTWiRtaUHWkfGj7z6f8bT1xetzIlcXmBiJE7cAJtwLR8ihxCx77mCcyaLgIj2rGKmBwgbbXSR3Z8nIu137wCLoHXj8b3vci9BfGPX4FfE2WdYPcPvf4WynEZ+s4eJRdC7Fj8mb19ZP6/goQAmPqHcQr6mVnkU1DwbeDwyf7njw4cPFYV33ek/+CJvrOvlJkWW+9Au9Zvt22Pn9s2eP4gvWE9XOnN/n1Du9DySeGE4OECiZ9h9mfiZ0Xpt5Q8F6nU2xdPaFcpcOaQ8mI2F7x34v/6jtk+gmxlpb2Fnmg5pVKPWiABNRJV9pa+BrtL7AuorcHcRoPdZtGIHhQhia2autapNuzj2r9Oz0moz03EVNlykxlRAzlhKbm82kLNBOe5hJvlyDoUvwOYVlE/UcPbZzGLexZ9dSpVxEsptlZi3Gqxsn64vGSVboCaN7vjpRGLytI2u1P0tVMWWCswnooipT4XngoVYUVGq/gHqtuo2Oo3iQoFKxvXlifjz63EW6sbylaWuZc1iG9kiKvZbHIYzAYORDejIqnDhmprJjPAgOpSBZlxciwOreX4yqlujlSiI6GLrqtmuuh6HoSbj9xNcM/PUZ5Ht/NjIkNOTsAjRtLzjytD16EfqGoAZIMxt9hzgyqNUzwbgTdpcQljvrqBYcwHf70HMZCRYcyR2k8qXg/coagAICVF8tnPDb3HER8ieWbljg7NFSldpPOsUOZR1J7Nv+wVEHLarBidnuQHy54LHVaL0WzW5kIARWHZLq9OjXXNGHzvGbinEyV5TPHn3x16+Vj87BlKAVGX4Rk6/Ez88FvJSSEOoGQVQHMNVlaXBLgO1RYVjInIuRZjhiavrW9AEF6zeodoKiai41pQ2DnAilu03cnR2SllACx2BO80OrZmqig3tuiqKLqzbo2iHsmTUWwIc8t2jKKVqxpdEkXRwhy5KQqAYhYw2Oh8HVy3IiY8MpZBVmLOLFNT/l0oriha2Y5EDUVhUmlHZJUaR0nC0Tk5ke44aZLLhXZl+S+SyzjIfzQYHnViziT/o2jBrNrvRNH0bnn0htEX3VHUQpUgTVE0ctYm5ujCBPJPRcw350TM4xMMuVxoOXrLWMvhV9WkC+S+Vsf4UBlL2SsHcrumblmZa60Cwt1kMxitTpvBZi5taHTTrsFiNah2DVOwbYO9sqK6Bg7HpMS4we0H+06FJUsU6atDu/bFzzyDxDjlN9ruNwOK1UuQPmwEWFLctNTiZksectU0WJEqWl2NPwIqgQJ/U4b/WUD/VWHo4Lb4wY8fRb6ZZeDZ3XeUWQzoGa6Q+EACkBMMCVK08UBuxKrdRUDZJBUnc9UtQ93flHqdsGmMVrH8R2L3UbSjZLHThpJwlDz304jio14z+tAtGjsSe/qiZBaLdtrrcU5UYtf6pI7be0e5VT0oyhUughms3ao+NFUmH1YaBpNNObCodheTqULLTtLvXi9tPDDxCaeJCfOsbGkfvZMaN1xX3tEReMzbXt2Bik/dXc3ezZ7gptpK6sGr3+9eq/L6C9T7+mBoSpPdhlVlKep5tb3nugt8JnKZrLtQHckTuTXhOoj+KGW/gkqyntA8JamXcVLnZ8qNDF4RDu2dD1fuqxGQ+eEo/rzGcw5RbATtE9qHhdf3zgdfqRoeoEXGsGAXHGHUQVSqOh15ERWUrUwY/0eReqvWV3jg5dHbMjNwFI+enh2Lb0TDCBU/0rBknQVwMems3JkueldiXxG9W1OPFRIvRSF1t0Z/RLsLJO3nxjXR0hQGEY7kywhROPfy5IDoYkWxscwEH0XZgFGk7reTDDkoEwlo5tYq6nC3lMmuRLwbDUZAR1H2022FIw3+6tT0vILxWGuWA1Wy2/VIJIT2uw4QPGg/etHK/lNmsyoKU6UkTPEQ0COGL0Wntfw5tfuP0n+t6v4TSUl8NaKNkiZz6rmso1NTXNVJeYIjdlUT4F0khNFMFowivSCrLT8EfAhifFh4ISwVaUg8j8IDEIVH96dRaKyXI+j3y9sv3UzKRiezm1zYYqgAMt4TGVEu7LujltxtRe0/pJagXLxG9RQxuaRmLVzISebnwIs7+0+dStRlQK8sAmj3f/g2kJLwotGXoWODJ5K9ZCab2YBpoHIMsER0yjGoS6fciQJCirxwpNdk4iQpL1wrj9ZUiroe2WeG23PLDB8nl0Jhpobnmkg7asVKkDg7SrUOkCUBTIRFOzU8UwyHxzI1NYPa83sXLLgERd85JXuK1qp0YCjVAwunVjqwGQrRu1uY5KxXQl2vdMeoVTpAgTcVOaMcsJRAWJc+gVBZRV0sHGrMIoEw8h+TC3j8NRXw+F2GtL/I3ygNngU82Y9eYPBc/5ROiux/UkDg/5bvL3h+lWoD/3dps2FtjoLHsElVQRG3m8jaG5esPSOmuiCy9j7RY99octEs1uQUZVf12DZD1l7kn5LZ9r8Q236bLvku0peGZ3WS5SLxbNLkIv3pebbg0+KisHIneHUMeTWKfoNZxFAjY54WtyKJQ3+b8FbU6KbFASBeo2xGzBuIWRnyJ30ToHT1sfTwbxFUDUj47sQ29/QC3bpApw1d5BEFn27SDZ5uTsun9pE3lxtbKECiuRymoUDw4RY8SjaUPeb4cGxRVKYjojuSH24N1VY1JcFqjKSBGtaWrSmrp2bb9CZRv9HgcFgcBjuaM7rRU44ApAAUWY2qEFXxKd5tu64V1dvIR4AGrjWhIrjhhwVEfJbv8Ajj8I8idRTbrxIWypo0afa2InLPN3AvfWOo5KEHWlzNS6wPUAhjZlPQ1+at9HZ7fB3CyvD/HBaadxffgPrisGFv4FVaFwCeBFeDOclHDrakAaKJHq64DcFjXHEbLsQVi3ir7/LG5CjXVExIDTwsWlUuzMl4MNjEIkBSr7UBw6cm2Dh0E7DsYs1HqtYnwe5Y5x3J68Srg+G5S01oop5w+4XFdk/AH8rNm5OhqnLfyOwgqvn0AIbfT6I3imc18HhW/7kYQFgjw9uZy0xqnFQSGGh1lp5B+AS5Z5BB6xIPq3QK6uBaeXgK7CVymS60IjpNEap6MB9o3Rox3gT9oZIEoFNREkynakQGNIvDMyWJA4sYRauX4miKcq5Gp8Vkg/fOQArPaDOjRD7rymWxqGsul3h5zeV1qV23Zw9u2z5w6Fz8vRfi23chOZvVoek/9T61vju7f/D8a2kbTBpp1yeVldCOWP84l6wRzXOVWveGR7k04DvaBagyrBnAXtOwDhUkNfF5rJYrOlUggM0bTmqV9kN5CfhfNYu5KuNoYsVzVNHNCRcy6h7JoNAl3Mfh6N1hUFVKkFqBO4eulqiGFWqGjiCmPnVi6Nyh/Jg6OdyWCFonYZWPhOGWz8rjr+JrxfMqA3E6rk0p6JauU2V0Uz5sLGUxomKDNhtH/yRf3rXZ7EaL1a7JuwSQTQeQkl2cqXARtYvzRkAsFbzDdpYp4A8lQEpaSEhrBvtApV655pMGqlW529TiEmm3mWVRhNFGT+XCJFGY7VujsNefjGL6c8S2JkefNw8j+pw75Uf96sCzju9Tl96V8NYR0TuSMbKj96hHiZRgSicKIIYaKQFYxvJoewatEcW1ia2WltDXgEmg5LEMk0C1Y23RHkUJZKmlvQoS4bt8RHghUud3Jo3hYsySOlHbFYpxWKgUqvbOKBlTsapBnUrgKXwGOihqhcNORZ0/z4k6J0RngYjOv0giTuAuRYt/ympgcCornUkWwx8x93N5Wevtq8ta3A30djXb/pcb7UhgM5jQw8FkQ2dN1Mpmbi5ER23YxDKsgN2u8gPMQCyIFaJciawWNVbgJrllQ3zfU0On30nJG/8ufASpvXzVoIMbgX2A/XIjqjvB9r8B1TWxr29Gr5eMWXNk68sGDHXL1tYNkV7F/v3WRM8GbLDE/XuGzrkOS26tj0Z9I6CdahqaKxGPTDsMn8pN/75THwGiiv9Y0x3YOoDb/DICWzGLmg6K1oWJHi0m3tVPo4OuuA5ymh18PguqA8GHfe3tXn8ycBWAmiNhwAhy2zC8EIaXgnkDJDbJqr4U9waA20Smyy7R4hcj2UagOK9KccmyIRpMEgrkFcheKLiNJnSw5lC9JIlw+UD1bkSpmASvE8tny+vYpFDry0Qqp57rTpVUrnTdRR9LycBzUm8pjQy8HEB5Sb1IRwzKi26hLQrKkT4ZRR/e3Ng2iqa9SR43dPB9OzwSlouio28WbrfoL7WzQFRethHxFWpOZ8lXKmVb3jhyZRtFwqh6twOB/J3420dfl0bhecusNpW8E308J6N1nJThWPANGvscCaMZSj6qKolvRG9kCoY1oaA4oY6DgkJtk7FiJCfCIHa0xNPQUVOHuNVa05qCcF0oAlpZlzGUElIWUEchIOa6Vp6SrgWZwUhVQngJdSnfKpVpQsUPGQ2IvCR8EQJSFRWKCQZ3tooJNQakHB4b+iwrK6cv+GKTZhmhqwyLgVDWaZGrpaZMufUGHO19orb5WeAcF92rXfgkujOLfMURaSpEqLNkMJUFmFFTfU8twCgKoWobe9EdCqefwrqLPvV9sOpGUTvpFQnS45XRs+qAB1LxCvzpmsm9BrZXqi1rKWtpquJvEugJICWcFnRgLHVbrBbePQ+lmlVOk0LaInHQBKknCS+h0kQ8l/dGnstLL9BE9mSYQ0o2kxWppEp3iFhESAPdEBWjmgige0K3poEz6LWqhZb/7vhLFe5BFkD6mCjybThpMMcIUDKiY2Tf8/nFRLUTd+F5UiXuouzVkTASD3K1G0ERvJaQ/NCKwKgej4oWJDiCM4Y+4EE0IignKvriSFwcf5XEtKjELkOfVmj0lWYVhVqWAfFUUVfH3yigTwaH1WBzAvpE5V9tFruRKmWiHqzBquLgmeheDQOytdKNELDa7XkbL3uPVOKBc88PbTs1eO5c36lnh546VqD+z1BlLuCJ243lZWvcpeWrXC0wGq0Oh/F2GV2lsGBvAxSI4FE8C3IRPkFuzENugLoIwFR3a2Wry2Qsh5cwuQmQZh0AjcwuVM23UFdcrb7XWjUA0rtXTSajIlNGhZES8VIpGClpTzlbsxAAHJ1vhvuHBaUvpgRJ4X4OpQjgZeYloZN1Tfk5/qoT/1WYCkC8OHWS/GgB0O7J0FXI+ueO1aFDO0csRcQAvMHJtg5Jzhs8c64iBIXDOWZKfu4sqvBzEQK+FNkzu03o0ZGIEKhAld6XRUj0Va1yZFkKD6vFYGMxk2ThMW2juPtcDiyhWngs4FtLoKLiO0+lFtotVX6dVJwsuX+X3WKUNp+TMMCsEtRu59EPkW1vQwUCLbbVSl5Gj2XdipsXFmpuPOeitq5eBdvoW8nuUXQ2za9LgDZbjlb7xyiagmyNHiUPKRybOYI43hstJosihKH0i6JVtlaZzej7Wew2R8RJaICj4iR42KUUtpr0KWwKjD3CjnabE11TxiuF7Q6ewkYv1L0dTkitQrMRRQqbImffmYJayjWvzYTh8oUp3Nrs8ppM5Mpk1UIqiUASkaJcdSiKDjEdKvFEgSS7oWCJNqNHozot6vQY3Gl2mB1OAyoRJDdxnYnSOuSldVWlGNm3D714vj96qP+lMNXvPA6Dey8sbE137XiiDGDQaVW1V0MMkqSNTtOH0GSXe62RcAQy3uykyp375b2GMGzGpAza0GxUr9Ypc5DSBhOCJ++E2vGKpPx6QuBkJXCin+Zun5NsiSKRR2k6fJ5j00tlMYcRNb1EASqVRMHWkZsO2ohnlNSrkbIVlPl5hDtHfT2rmK1gt/OSwMkl9kYPOHrH3cx4uJ0bD7ehbZYW4jkr3GgIKQEWnSIlqWBn8sx8t8HPXNDOYkoALU2moluYyR18d/wUXkCkTw8L2Eo+I+xKKbrFYM9ojpKbViWFygD//GmyAPyLfi3QHK/mmN2mNLXmHHJJ5GQrucCcqnBcWMoAQCmG0eYHi9WKAora/JB1kZNMGQB3IANJwQ+ImGrwg3ILqV/vl/jBiOCklm9Hix8c321kMhd2iP7Zd88F0X+THYL1N1nsGkdE6r9LInXkX4q7xkmtjWvqqpIsSqmIXGtKETnTFVJE7k60TKcNKc/7ppdMpa+ozhnFJXl1MmX+qDLCkloKzmS1f4fxEx0oqVQKjoqQbo18DOdKBB6N3JwrbANmLTOIiAi9rgtZ14eTs8WS68PRQ46gPlxUEQgZWXE4SBSVwkCRbYmLXGm4yJXMRSanU1mK0Wp0FGzwMhVMowxeWsEM4CIzD0LQy+hwUeiHgL6KadeI5uEfHpxMH1lAB67vnNPC/3pJeDyiDiqwKodY8xGyGrLZx5rV2EMWBKvBMaFiNZT+kWKOUFgpGRc7fkwhjXU1Zc1l7nVr6U3r7VW3c9BAOXKIbcCJIefCRmRldrKcC7vDbMF4bQy95kzKtP5n5hdZu6w8JX4wY+Dw7vhLaHe9c2g7zOLloduH3j2EvNv4x88iDkldTs4fUAYKBLRMMZgsTmgMKs2IinSKOEFKJQVK9XxLLppQwTtgVyCEC/Z6APkfYK9aJEApY3wAtUXRshIVE5o52rqZ67QmFE7A6Q/Dg6GuQ7wqNRIINAElVFvRpzuRGsq7s6kLJVj0wgmhmxOJFhocm1oqAWE2BbOKYcDQ3PKe7o2BoO8X3sb161FFogGD9YR4jQekAoi+FFZQQS9YOD5ouNBciapkoqKY4TqsEeVhfxqJvxZTRAvzSMbAmqi7FGknY/ClA6yDLR106NthGHdH6E/uWzbFWLLo08e3bOA1keX0t2zKJNZ/GkkcEc1HtcVC9K/VJmweAsGJDavVKiZhJQmE6dhWQiBULG8GXEcdULyRoxEG9709cPSoKqAogllNyhM0Q4qDJ9/uO3s2vvPD+P5TBgc6S0FgiahW0r9Z1XKthJAA81dy5q/IAdFqLTMl0qqkZmQazH8VtG8patVobhZT7V6LKUcYK/k7H+51NVANmWsbPN2+gN/T4eveIlbEdzXISU7vvDd0CPb7IyzJ6c0wMvlHHPuP/iPtFlH27ckoSr3lmIfxbUqUEbXg8ubDKIrCZcF9UTHJNG2FhBExG5SgitlQ3EvSwcvT6WDX7c23V0tCYbm2Dray0KOS5aw2dWM/xnLRgeRUjEFKPB9K5o5UjJ06IlDFDdUqnnRY8VJmvHcUo6dEiVRGiP6vivxCLfyLNW0PDBgdihpBYvMv0mh6fcAYX0T/dVh4J4r+wSMh+lTjNFmLWcFQo80G2CUdCeNP3uqIHqogGAK0p2IIEJWUB9xqam5as7JOJzKn8KMY0Y7DaDU5WXk7h91ggZNZo4HsDKoQPirVY6t46J1eQPqV0F/Zht4tZSbTiEPvFos1777I42TKTZXXJP9IWMIZQnmLCnNMw2qDyUbWmiq9MTfmeCEsBq0VT14QFVoRatXmkOj/kSNrZAhaR88kK4az45yjl1UkOon53NrMF/1MC5xpSGG+KA8dqnyP59LVnATzFXz4GQHIfGvDRv97iuH1P3LVM5yVomjV/R3HaC85vtn59PD922fwDPoVvq5uGMIl/BPevfQ+GcZTXZ5g0OcNUp3Fcl+ww+f3ugLt3sg8VnlRPNLg2eyNXE1HpsF49nn8bV40sHrn/fiB3WhhNnD4jUv4aha/c0PP5oe9wdDk+lVmIMLomqsaO71BmN/+DeJvCe9HsJv8IHqJLpsvf6u6fpJrndlopstnV3o7PcHunqDXBeNduVmi56Fj9HihyRxoSVfMkA5WNYRK3Bs9/g0bPT72RVMw8KgPT19biTDlJDo03RXo8XcHt7AjxWzQ4hHprlQGgV1c21UZ2Ozt6va1UfMoNj2Yukc9HdJzFaM6AipqYts6LIRDk4fefSlO2+YIHsIb6OnoWI+QcnTvjHsfGhZi+DV0MTqJs954n35A+ES86VXycDH5nYEgLdW0Fm9wsw+7l94WI1uFJhynZQoVNa1eJh9hT3xV/yfvUuM33gf3tTPx187woeMSOgFzMqVplVDZ2LCMxjBHHIP4Y8LpUFEttkzCmdBVfWe3xz99XXUj4WxoSm2Dq6aqsYEuntvi2eQNrF9f6en2tvg2e3mvm18suPDcdQC60Ani3ZNP+GLhPH7C7Kqf9/g6N3v94nDMqBJBA6zsIYoJEOS6i1Min+pKz5YuXiSU9XEJlTzc4+toB2mRW6fIhnz9S8K7wnvEg8dAZmhZdhyEy2hN+DA0qX4VwqnDwgnVljscQ3+TSAz5LFgcUAVbHEjjkzE419iyCL+mC9jyxlCcMxybShegYFnSasZgdvMLPhUA0jgdhhTARA4eOt1/5GjyRJbUYAlW1pY3XhI+Y2eHipbVI10HFwzt6sUqJl8waVltfRPO/jwxzQLhic5Jn7/gqN3zoSKzGabkBQwDbs8vFPMVQ9s3zBOVJOXzhKQIjXkC8wLhBd/PR5hDBHPY9H0cmlRX5XDCFT0u0xfDEp6OoTHtmRgwlmdj6H/8GQ7ggc/E0GL4bAztrBUz0ZQ0E+wzZsJuX8NnAnXtUmYiNl+cBpyrPQ1omDhe9JLbgA+XJA14lbz0iCvSgFEnTn/AIGW9AdsKcsBfzLpDPeD7GJbufOwHfLQoVKE/2uv0qZz2ghpUjm5RhUPlMXSrSjB5HVv587HbMg/8A/1VJgFROCOUJdi9D6hX+UEm4Rgfp5FoMh+jmpqeOLNrLzSOFuA03GdVT8Mm9lkm9rSyDCmw2nPgdFDgm4gdQTlJpFfXs6PjMge9LRBNKTKdVJ+OWP+iLkmsP8GEAJGDGYER4nu4szUUnF1WcLAAdGcDFKUxG+PKGMxCiKF4xNnYAmYHHEDA7nTsBjoKYj4bu0mh5b7YlCT0n+NaTpKBgHGlzsVkFIqAV0hUdGiyqj0dMNZBFTQdomlO+h42uA1Hx4U4slJ/5D1NSMLnuM6LQZkRKYCM9VUAEqP0FJ61MBXec+rF/nJWlWqxtSw7WQ7CbNKTg9TAWa3wJtdVmRx0eFzWWTKLlUSfhuTZTiKx5l8uZJ9JAligo3UNu1tEegdEmSYCRHw8gn4HfB8QQ3/jcRkro+kUFo/9JHW8MQQSzsWQU5uWmWMV4rCQy5E6rNDk6npur9L6QvBxGR8DanX8RpvJYo9hRc7F4Iw+H7srCzsVDR30BkoWjHqgiwtqoHj0czHsoc7HsCJ6ezGZY3FOKsde1kuH5dEY0eW8RgCea4zo/iQr4vKXLlCkNE5UAzwewb7qA+BPXnlj4P0zoq+PHCmNsAwK0nS+8FyS6byQ7xOhMGziDgIxBX07AYAQ1dhjgJqI44XxWjhyNOHzSdox1cnjtcMNmdERAFWoN96C2gPL472P+bgSRuCDohEo+gHSLi70q95gx8v8ycrMlQe7KcnBd5hbP7F6vrJpBwukocZgJxPnmiDJxo+UszKAIsg6kRf1y1nqjY2mK0uyhVBVg8aJbcvxyIuyiQf/7fiNMKPRA2PsXAx+aHmvknblwLcaI8J6Fs6I2EYsBkRNdrsvlJ/SMOMmwV4ld7zaioOzblzGmd1GJAZ3yjlywMmGuebGS9pvNOvbq7TNVI8UlSzHZaRZyZwY+OhcDF6V9AOVPOZACNOKIjhwnDI9xI0VMIMFNCLs+c/FUEdL9pWm9ZvBk6QxInRtKaARkSM4hs1fVt7fMChVY0Tj6t/KKBlhZJyLQR1l59uEh0tjRMvHaY2ylBh1NCTIcFlNa5KdJDGg2zQ2FZe1EIRy0xjS5SztUF8mVSOj3sM6ixUgBbU/LXY5C0Hs+TQGCkewPM4rw6+E9iAafsHxCoTEsHXOJg4Sg9/yXAx+y/Mx7Ev1wh2xPxK9gki613OWUVBXbXygBkIBKTakF5yLARCTXTx3l6aAuZxHhGLTGiJTdNXL4brYZU6g+9koo9jCHo9KcgTOldUNVlP+VgnMIQJdLQNoKRyaMfjUkfj5l/tfeT5+agdBr+bUBATXRp9Q7/NvFAioxVE+ABARygfZYIBgDf56T3zfy2roTrLhwqKUQBkRnEfxEyLACrioOTWNgqumVqivbagRXLUtrcrA3YMEd1JGriQnDbDajLGLATVKdUpJNgKaHGvPm8UgOeGKaRroAcMx1CqMxGApJY0zhuaFI0IzseHH0P3wbKxDwizpBG6/vI8Fq6QN/Q0XNiXwOKBhGjAEEg0Y+3SG5GLIpJg0YORT6BAK5RPlKbcQ+6R+1+WMUCb3R2PxZ1CjJBKaVOPxB9LNFwdzaVIE0uHPwD3K7pWghckNjbUV5bUqCkiKZD/HKIKgWiYxPPDn6SgA1KlDAXLgrpjGF8MDhWMwVyIxFF4ddQqg4cZgzp6NbclIASq/5A0XHhRdOjAOSZEBV6u//DDoabQgtePUaBMO6PiJcPzjXX2ntsU//jVzyNaVO/InBdb6PIq9OkNpUrYt0cHUmoB/g1CHP3zmYMelIgFBC5rbaX6U7kn4PZ7Bq4RgQkJMJcCfUEfASyVyLcl/u5AjXqSYPmq4psb0i22lZqeo9IEYpLmC7jwevRdzFdvG5se11mKUHfTycGMkE2Mo6qs7vjyRjmzgMRggZ2Poj8DxjLqgjiQkF4eyyWGyv0vHByhxraIMcbSgBurHkZ9gSFBD5unJYvnlWfg8Bnj+uRiSuuXVBJ2lSvnd4krCU6G3krJ3urBWchNTYwm3LUP1JkabdiWRN6Szks68RfyYraTskBejS3x7L4YctFg0Ji0q2m7rLaocTyqkRb1BLP2bUN0PitgzUU2h8o++oQIlrseeeeOIxnxRv5yVRMb3iQJJXF+kGuozLbg7dX1j+yRZVFBL+4WaX2/4Mhd+fUvL9RYCdsjqICNUhRyLXc7RF3S7TPUx8oFKYOErwwcC373eQI0i9l/GAsYu5+hTVNepZRAxj4kFvZy9lKgnquFEwFb3ymFOZGlqDBE9f9VDvJypNcY1CtozHY/+MbPlG9YazXlH7IvgOcS2t74amAHa9p6LDb2M2vPY9tZ72jzY0NFWAJEyza2O3rb3Db7tZfdKbHsn1Ze7ylepAMtJm5vDoi0BNzNtAjXNYckHgApNKfPgoOrK3MqnYcXwHOEY7K9I7EiageS5p6G7n4lB752Nva2zp4m9oHRyPMhGez4mji6t+YuMv1RLKQZHT6HtYdCMSIxmZWPuntM0h0IFZQ6xRYtBtclbzrQ27ZeaKwWkSaGtFJxm52JwFMkbk7SGKyqJFrzhGnuFhnQo2y0lPGg0JDQ/Pk7Njz/g2L3JrnJklfIoXW8N8ic/Dk2uaTSORKQiVRQiFT3BmUhFInT/SfQTiYSmtCA3+hEfQrokVHGWtv9IK/pzOoZkLbiO+s8eQxZzStJtS215w/LaBqXj6Ikk2TpL3LWI2RDg29RdixQp+neaMzXJBTeKvPmmQdJDhWMfkoD9KM2g8hWwNOQYzNCzsRMZnUbcgx5D8xaW/iNiG9el242ijpM2PVhsNmy+iR7cLkYPbhc/lCdLIxOY6KEcMCfyKL4eikfBe6CHlchMbvcgykD0gKQMfXpIBsmdjsGFRvTwyfa+M5SCnsjmpiTslbUNyyopdVhOgf1iljhDBHihGQKV6WYAF5vRx0d0O/1Gd5bs5JrGLMXgcuWTxI7wSaIx0zOGY6eJPGCY6Y4xX/KgGYihDMfZ2GeZyOO+JJ/ig2KyDPQRTQKcpPpOC/SQTCWTGMwPaegxGNYjoIwsZ4lciyQZtClBnofPYzAdRFiTODpNRNp5cXn/So/VUUeyoNYyxiwNhAhlSKFWbp8cIkKxdV3m5v6IBNmiRFIhrWACu81TvGKQYudj4mJqIkOlxYShokGqUOUFxJM5riOsdxoS7KnjUVgwUqUGV63VkLdAnoRoHgnkJgBUSUHvD6NkIgnkyS0eX6dXjPVhS5W9fua8GYPipI3Pkafihz9SxH4h+oXWVeUNQnltc1Njc4tSLt+blLR5nxgIE/O3NZM2i+2lTofIwGhSpj1BRlljsfHSs4Vjf0HC+C+5MNYcYL7CmEYe+4qE8deZhPETPNbJneQiWWuaI9JWD/tRPRrIOwkhlzmRRG9KiDex6PLQEwr3sDqk++VCcfsnLqwmaP0/iIv6qtaIYxdFRs7l2cd8PW/4YpMKvXDDF9IuXtwQpl1a0CMtLczj49RuT7TJJ7Uss9nydmkUI0AL9ka3Se7SOPzZYBgqGvZWBWr9bAogbqyIfANZAYwH77qpwHhoYrBOx9Ax7kzouoGTb/ad2jW04+3+Qx8n214z3KtWlzc3lFfUrKqnED/hsxQhMF6CRcz5K0ZARN8K/1p3bsjRzEQ6Gyo9VDj2t8TZaFSXNLQY3Ax5wzr4eGP/mdj679MVWPnyPjUNXJAqEvCoUDEErX44Hy0GyWmnztDg6EnJASsXmYldzoDrb/SCfSNIo881/KUJLUyAExLRadkKSdpIfjkrGwSCPYFAgNTS8CRc1j4fNGbQC42IO0dFfvVljdVGYWG9kTpE+GEiOHI5J++iobPOQJ0GMdqVwFle9gUCUFlWd1nF0V4ZMS9UVdblVNiOVw7mGaWaScqiovhxqiguOTfX1ThNzrw3SkUcE4l6+9ySYm0GeMXCjV7/L/A/tzeAYslhq9TbAs/C6RieGA6s+Mevo5VFSvE7d01Vwzr8TwYUfLUJA+peMUR0jtUBdHcHOsXKlZPpPVWsFEsVhorqauCAvRC+gEqwZIMAgJcKlZuERvMGydX1B905tDtkaDWDUeLZwzGcHokNp5mBfG1vmpnYfyWj67+l30vdcOGwGkb5xX1iurO4rYKi0nds/Z/6dpeUdpmwu/KGz+VYFjG7ZMSs08JQ4HRatSfoJSK5f/tkenv/9gsLPhi+DnUYWfVTHHEHeoJt3g/wcVZtlysQ7GwJeh71dvAz3j2BP0c/xp8LDdDqePlT9jK3iSpOdnvpBi1bOr1gg/mXhH8r/C/hffzmwp8DKvLv2Lu/QCTzL8MoaPIVlZr5mv78e5yP4k10/o3pzocY/pqMCTp/IT//TZ3zcdevqCrD1/RHcf9bdM5HKamvyOr+mv4ozo+lO1/5/OLzzNE5H5UVv6IM0K/pj+L+P9Q5H24uzfOPpXse5fyI8+nTOR/Yfs3xXkg3nxrrdTHd+crnKeLr9Rvt80NFK8vQW/Qrynf4mrIcSldDXiXm6X3pMoRBFGQUKlpRBs/JVwTpxWV15eheyS8Tye9nOo+HPbZ6eov541nTDUeDPH6rcz78/F8RpSaT6616w19TRhRCSAaMo2FtaYM4DnEZF+iNv74MWIWvCP7Nxl8qjV+8rknn9ya13r66bDVdiQggrnThSlR8UBDmQr1fbClDPvNX5ISk62pLXWAduk6cwZ9r/2IMiucrcm3gopZlpS3IoFb82O5006gx7dv0Hg5NcxPk4FqbPB0HtX8HLKlJDYdTTw+jF8Rfhv9F54JPdKZ7SjPviUXPBqsEc7CuppT6FSnmYIXOj4Wm8J4eAPrzi08Wocir4kJzmkGp525YLHM9p8X7eHezd7MnuEk0GGYkjgjdH5A5UUJH0PflVuozhDLNS+HeGdr2at/pV+Ifv/WHz141GdAzp/+ld/7w2WH5dNRLlk83GeqWCesDQcEfCG72dAidnq4ur3+DN9h1SfgPQncYEb6LoR/2nX998MT2+K4zg0f2/uGzvX2n9sX3n4jveXPw47fi75+MP31+4Cgcnr/ll6Ce7cXQPdLzCMoHWmrqO/vr0v4T+/FiMieeTPjtjgr5ApPQVFvlqnILjdVCQ6NQ39hcJbQgx0UwmfGsVeWuGu0HFjBG/gSIF+s+gTGLJzCO+AlguF18qqio6KaBFw/FP3p34OAb327bHt8Ti+85gjcUDu99BW8G3jvbH+4d+OjZgWdP93+8Z/CTpwdPHkODNLZ2e4feepom+Nl9gyc+pUJFsbM43v/avv79B/rOHUYHtf4Xd/fveWdg53t0q4/OYYn6d780cPhUfGds8MwHfWc/pyOvPD/w6tPxd/bF94UxdNw2vi+G1Rs4GMNt42/9CsuIJBR8Fd+5b/Dk6f5nX0F7Nqxz/+vPf7ttB7rZ98fQ0/7V+KfR/t0H4nveABVgtfFDONJ35oP4/peVD49r+85+il/ko4i/vqv/5Tf7Yy/SaQee4c/Gf5q1fhJJBkxzMfQIJyW6Mwjqg6OchvkkIDCBH40feB7PT7Ox6+TAwU/iaHL0xvu8hRifmb5TH8XPP2UxGGxWB/2DmcL04RZD28QpHnrnwNDbWAxkjYi/jUDqxdChcf1t/DwngaEjZ8XFZB+Jx0J7+ZLKK7YYtdV/G1rS5UWlc7/wmK97o9AW6PR5u4TAesGNox7/XV0C6tC3Cx5/O2NgqrG++CHOC4ijXAytYMy1rGGpmhuJEc+cUYiIrTiHSZEM5+GJdjws37PJGxTwf1fAL+iLE+LOxO2zvoQPArEFcRCQA5kebivOyWkQ7J5ZP1Hi9llfwgbxPo2BSTm06ksjprdSd+8MklySl3Qn/SmX75RJyMMCuhhapSnVsWri8X27BndHZOE/cPLEwHsnhnbtHTj2HA72v7at/9Crg598OvT2r8Bx5nh4f//Lv+aiQuY0RA4uhkrryu+uK/9j19p7oPXFd0s0jpUaTWaL1WbnBIBkapEAgPrMSAA4JycCYPfMejW3yrfP+hI+CLgcLoZY32ism1o3S0uVpLHB+ILy/E0biItEAmHnsiP89ijrc3HH6xKBsSm9h2aWplqczFKh9D6JvNjs37MELzjhvvjOY1jJpQYD/jOWmSz0soR9MvFPJv7JzD+Z+Se0P6JPFv4JXfLok5V/svFPNv7Jzj/Z7+PP+jZNxWp6VkDOktZTW0NvxYkai6qvznfLdx+JOk/87EjuwgeNvQIzCkpko4A456UwtwJIMzMDgQ4ydaBnF3C2ApspDQHoDujwy1T/I4x6cUe9tGCucvivjWVO6PHbXeVGo9WIVlAGwxJXucVgt5ZZDE4HvbehoacFjVTxHv1jjWVmXIX38rUiqUH+XAzZEvfO6X63izdBQ1iJXssbs6TX8sY86JXuPhJK2wrcBv/ZkdyF0yt2udKq4LFGd1V+mlgVdu+8VgVeiIuhB9jSNlUlrYrSqsG3Gkuh3BpBoTIbid0ngxEj3y39eXwOkd17MdTav3c7maaHPyeT9dAz3MLjFvIQNCez5/lX8chOyIL4uW39rx8FyKf/9Dns5mCNk2m4f098Jx2k95I9z6zDHd3LetDFp0vYGGCdY4R6j8/fQdYg6kT4PbJN2CVs7unqFjqpZVC7V2FGyuc3e7vwjR+3AqzoMY9faII5yexK8XMVdRYiXbnZJ5uYgGde3LFfmj5X7d2uWpXGkSaMfzPeqgYd10XTAegKTdPBnNgjb8U5uqaD4jzZAGb3VNgBZt39NDOA5dtnfQknI1R1uHgA2fu/PVl0Oz+CLE2Ftlcp84LT9sjFycYjYDbl6hEwm7L0CADQI+2C1q3NaD9Ss+acdkF0z6yNwa3y7bO+hC85vEQXQ9Olq2+XdBMyCjKMTUng6camReA4PxcC1xpbep7gYwO4nMZG21T8II0N8YprsL0Ntm10BX3d3qDP0xLY5PXvWFNXv7yxqrUcUKWapnpLFb3WNFiXN9evWNnSvNq2fFWLy2JdubwWpzS51q6zrXS1tjY3NVStqTO5ljc0tK5yV69zramvNVW3NtfXrlvhsjW3uqpXVpaX4yfnl7e3U5euSm+3x9dR618f4GGYeSnHhSXwph7HJTPRgq29p421IgtdQ6bbOy8N7QzFz7zc99kr/U+9Gd99CmddJZ7lRu92fupUfhL/emalt6st6Ouk3wYac88nAweP/eH8UZg9+HJyc0+HN7Ro0bfbDnBHyMBzH8L18u22XqvBEN+1H2cpf0p6IArxhG087sOe0IXYz4ZAcAuFjaRjbprZjoZAqNhKpXfwJK7A5k6Pn7Wgo2iT29Ph7ULgqM27e1JxzYUiKN2rWJWb+7dPX+X3dau+un97yRov9czbXfxHyNI0sPbTxo9w7tXlfrSZ83XiEdpr/W1oO2fE/W6g+2l9aUr3pTnx5TVJQ+Mzq5r4Fl/bJi/8qOt7/O00kTt2PrHIDS3Z4U0cW7T0iUX8kyvgb/fRKnQtWvqg6adPli5Kvp7OrfD522l6Fy01li6q7aqHWuWXL1q6qHpRqda9jD8tXeTydFR7Weht0VLDk09eEv4joyKYv38tUc7A8+/3nT6QGIDwu9B0fgyEMgfu2L9RkIeROpr9px3/eZGRvIqvfShfxhx8x/rOPA9K6n/27f4DZ7C7G3rqWHz3M/CG9Z19hZOp+GNn3ou/sB2ePtrpH/hgja/aB+ccnFTxd+H/exXewf6Xj/afOoXf6DvzDuyR/uePDr0fI4/m5y/EXwsPnHue+dS2933WO/RyNH4sgqelB3opHD+6q//5t4YOvU8nv0EVrHCfwfd2Y2uD+5N3c9+L/WePEBnvfIo2Pif24wg9/QfPgbBtdJg56/42DCXye4SV/u5kETA7gPf9fRI5Cv8gH/jH3ZOKhAsgVJn+hH+Sv/wv8rtv5Xd9J4uWDAvxAqKMSxA5K3u8wS3UhJBaWHIxNFt1DIHd5ZE5xD/scNXj3rYeIl26hlUEu3/7nIYAbLqeDpCnBw46OnpJGBAGI0BCDfGqYf8sHwxNWeXf5A88BogEfQc3Ov/u0vD/D9CJ2n97LAI=";

                try
                {
                    process(caseID, responseName, mockResponse);
                }
                catch (Exception ex)
                {
                    fs = new FileStream("C:\\Log.txt", FileMode.Append);
                    sw = new StreamWriter(fs, Encoding.Default);
                    sw.WriteLine(string.Format("{0}     CaseID:{1}      ResponseName:{2}    Update Fail", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), caseID, responseName));
                    sw.WriteLine(ex.Message);
                    sw.WriteLine(ex.StackTrace);
                    sw.Close();
                    fs.Close(); 
                }
            }

            fs = new FileStream("C:\\Log.txt", FileMode.Append);
            sw = new StreamWriter(fs, Encoding.Default);
            sw.WriteLine(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")+" 结束执行###############################################################################");
            sw.Close();
            fs.Close(); 

        }

        private void process(string caseID, string responseName, string mockResponse)
        {
            

            string decodeStr = string.Empty;
            bool needEncode = true;
            if (responseName == "单推往返接口" || responseName == "TransportListSearch" || responseName == "TransportDetailSearch")
                decodeStr = mockResponse;
            else
                decodeStr = DecodeString(mockResponse, responseName,ref needEncode);
            decodeStr = updateDate(decodeStr);
            if (decodeStr != string.Empty)
            {
                if (responseName != "单推往返接口" && responseName != "TransportListSearch" && responseName != "TransportDetailSearch" && needEncode)
                    decodeStr = EncodeString(decodeStr, mockResponse,responseName);
                updateDB(mockResponse, decodeStr, caseID, responseName);


                FileStream fs = new FileStream("C:\\Log.txt", FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);
                sw.WriteLine(string.Format("{0}     CaseID:{1}      ResponseName:{2}    Update", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), caseID, responseName));
                sw.Close();
                fs.Close();               
            
            }
            if(responseName == "政策查询接口")
                decodeStr = mockResponse;
            decodeStr = updateDate(decodeStr);
            if (decodeStr != string.Empty)
            {
                updateDB(mockResponse, decodeStr, caseID, responseName,true);


                FileStream fs = new FileStream("C:\\Log.txt", FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.Default);
                sw.WriteLine(string.Format("{0}     CaseID:{1}      ResponseName:{2}    Update", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), caseID, responseName));
                sw.Close();
                fs.Close();

            }

        }

        /// <summary>
        /// 更新报文中的日期，每次增加7天
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private string updateDate(string responseXML)
        {
            int timespan = 0;

            MatchCollection responseGroups = Regex.Matches(responseXML, @"<DepartTime>\d+-\d+-\d+T\d+:\d+:\d+</DepartTime>");
            string responseXMLProcessed = string.Empty;
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                DateTime dTime = DateTime.Parse(departTime.Replace("<DepartTime>", "").Replace("</DepartTime>", ""));
                while (dTime.Add(TimeSpan.FromDays(timespan)) < DateTime.Today.AddDays(1)) 
                {
                   timespan += 7;
                   
                }
                if (timespan == 0)
                    return string.Empty;
                dTime = dTime.Add(TimeSpan.FromDays(timespan));
                string departTimeNew = string.Format("<DepartTime>{0}</DepartTime>",dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<ArriveTime>\d+-\d+-\d+T\d+:\d+:\d+</ArriveTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<ArriveTime>", "").Replace("</ArriveTime>", ""));
                aTime = aTime.Add(TimeSpan.FromDays(timespan));
                string arriveTimeNew = string.Format("<ArriveTime>{0}</ArriveTime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<Date>\d+</Date>");
            foreach (Match eachGroup in responseGroups)
            {
                string date = eachGroup.Value;
                DateTime dTime = DateTime.ParseExact(date.Replace("<Date>", "").Replace("</Date>", ""),"yyyyMMdd",null);
                while (dTime.Add(TimeSpan.FromDays(timespan)) < DateTime.Today.AddDays(1))
                {
                    timespan += 7;

                }
                if (timespan == 0)
                    return string.Empty;
                dTime = dTime.Add(TimeSpan.FromDays(timespan));
                string departTimeNew = string.Format("<Date>{0}</Date>", dTime.ToString("yyyyMMdd"));
                responseXML = responseXML.Replace(date, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"""DeptDateTime"":""\d+-\d+-\d+ \d+:\d+:\d+""");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                DateTime dTime = DateTime.Parse(departTime.Replace(@"""DeptDateTime"":""", "").Replace(@"""", ""));
                while (dTime.Add(TimeSpan.FromDays(timespan)) < DateTime.Today.AddDays(1))
                {
                    timespan += 7;

                }
                if (timespan == 0)
                    return string.Empty;
                dTime = dTime.Add(TimeSpan.FromDays(timespan));
                string departTimeNew = string.Format(@"""DeptDateTime"":""{0}""", dTime.ToString("yyyy-MM-dd HH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"""ArrvDateTime"":""\d+-\d+-\d+ \d+:\d+:\d+""");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace(@"""ArrvDateTime"":""", "").Replace(@"""", ""));
                aTime = aTime.Add(TimeSpan.FromDays(timespan));
                string arriveTimeNew = string.Format(@"""ArrvDateTime"":""{0}""", aTime.ToString("yyyy-MM-dd HH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"""DeptDateTime"":\W*""/Date\(\d+-\d+\)/""");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                DateTime dTime = ConverFromJsDate(departTime.Replace(@"""DeptDateTime"": ""/Date(", "").Replace(@"""DeptDateTime"":""/Date(", "").Replace(@")/""", ""));
                while (dTime.Add(TimeSpan.FromDays(timespan)) < DateTime.Today.AddDays(1))
                {
                    timespan += 7;

                }
                if (timespan == 0)
                    return string.Empty;
                dTime = dTime.Add(TimeSpan.FromDays(timespan));
                string departTimeNew = string.Format(@"""DeptDateTime"": ""/Date({0})/""", CovertToJSDate(dTime));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"""ArrvDateTime"":\W*""/Date\(\d+-\d+\)/""");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = ConverFromJsDate(arriveTime.Replace(@"""ArrvDateTime"": ""/Date(", "").Replace(@"""ArrvDateTime"":""/Date(", "").Replace(@")/""", ""));
                aTime = aTime.Add(TimeSpan.FromDays(timespan));
                string arriveTimeNew = string.Format(@"""ArrvDateTime"": ""/Date({0})/""", CovertToJSDate(aTime));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<d3p1:TakeoffDateTime>\d+-\d+-\d+T\d+:\d+:\d+</d3p1:TakeoffDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                DateTime dTime = DateTime.Parse(departTime.Replace("<d3p1:TakeoffDateTime>", "").Replace("</d3p1:TakeoffDateTime>", ""));
                while (dTime.Add(TimeSpan.FromDays(timespan)) < DateTime.Today.AddDays(1))
                {
                    timespan += 7;

                }
                if (timespan == 0)
                    return string.Empty;
                dTime = dTime.Add(TimeSpan.FromDays(timespan));
                string departTimeNew = string.Format("<d3p1:TakeoffDateTime>{0}</d3p1:TakeoffDateTime>", dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<d3p1:ArrivalDateTime>\d+-\d+-\d+T\d+:\d+:\d+</d3p1:ArrivalDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<d3p1:ArrivalDateTime>", "").Replace("</d3p1:ArrivalDateTime>", ""));
                aTime = aTime.Add(TimeSpan.FromDays(timespan));
                string arriveTimeNew = string.Format("<d3p1:ArrivalDateTime>{0}</d3p1:ArrivalDateTime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<q1:DeptDateTime>\d+-\d+-\d+T\d+:\d+:\d+</q1:DeptDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                DateTime dTime = DateTime.Parse(departTime.Replace("<q1:DeptDateTime>", "").Replace("</q1:DeptDateTime>", ""));
                while (dTime.Add(TimeSpan.FromDays(timespan)) < DateTime.Today.AddDays(1))
                {
                    timespan += 7;

                }
                if (timespan == 0)
                    return string.Empty;
                dTime = dTime.Add(TimeSpan.FromDays(timespan));
                string departTimeNew = string.Format("<q1:DeptDateTime>{0}</q1:DeptDateTime>", dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<q1:ArrvDateTime>\d+-\d+-\d+T\d+:\d+:\d+</q1:ArrvDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<q1:ArrvDateTime>", "").Replace("</q1:ArrvDateTime>", ""));
                aTime = aTime.Add(TimeSpan.FromDays(timespan));
                string arriveTimeNew = string.Format("<q1:ArrvDateTime>{0}</q1:ArrvDateTime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<DeptDateTime>\d+-\d+-\d+T\d+:\d+:\d+</DeptDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                DateTime dTime = DateTime.Parse(departTime.Replace("<DeptDateTime>", "").Replace("</DeptDateTime>", ""));
                while (dTime.Add(TimeSpan.FromDays(timespan)) < DateTime.Today.AddDays(1))
                {
                    timespan += 7;

                }
                if (timespan == 0)
                    return string.Empty;
                dTime = dTime.Add(TimeSpan.FromDays(timespan));
                string departTimeNew = string.Format("<DeptDateTime>{0}</DeptDateTime>", dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<ArrvDateTime>\d+-\d+-\d+T\d+:\d+:\d+</ArrvDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<ArrvDateTime>", "").Replace("</ArrvDateTime>", ""));
                aTime = aTime.Add(TimeSpan.FromDays(timespan));
                string arriveTimeNew = string.Format("<ArrvDateTime>{0}</ArrvDateTime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }

            return responseXMLProcessed;
        }


        private string CovertToJSDate(DateTime dateTime)
        {
            string ticks = dateTime.Subtract(new DateTime(1970, 1, 1, 8, 0, 0)).Ticks.ToString();
            return ticks.Substring(0, ticks.Length - 4) + "-" + ticks.Substring(ticks.Length - 4, 4);

        }

        private DateTime ConverFromJsDate(string ticks)
        {
            ticks = ticks.Replace("-", "");
            long realTime = 0;
            if (long.TryParse(ticks, out realTime))
            {
                TimeSpan span = new TimeSpan(realTime);
                DateTime StartDate = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
                DateTime date = StartDate.Add(span);
                return date;
            }
            else
            {
                return DateTime.Now;
            }
        }

        private void updateDB(string responseXML, string decodeStr,string caseID, string responseName,bool skip=false)
        {
            
                string oldStr = string.Empty;
                int start = responseXML.IndexOf("<string>");
                int end = responseXML.LastIndexOf("</string>");
                int length = 8;
                if (!skip)
                {
                    if (start == end)
                    {
                        start = responseXML.IndexOf("<Result>");
                        end = responseXML.LastIndexOf("</Result>");
                    }
                    if (start == end)
                    {
                        start = responseXML.IndexOf("<q1:Result>");
                        end = responseXML.LastIndexOf("</q1:Result>");
                        length = 11;
                    }
                    if (start == end)
                    {
                        start = responseXML.IndexOf("<BodyEncoded>");
                        end = responseXML.LastIndexOf("</BodyEncoded>");
                        length = 13;
                    }



                    if (start != -1)
                        oldStr = responseXML.Substring(start + length, end - start - length);
                    else
                        oldStr = responseXML;
                }
                else
                {
                    oldStr = responseXML;
                }

            responseXML = responseXML.Replace(oldStr, decodeStr);

            string SQLStr = string.Format("update FlightBookingAutomation..MockResponse set MockResponse = '{0}' where CaseID = {1} and MockResponseName = '{2}'",responseXML.Replace("'","''"),caseID,responseName);
            ExecuteSqlByScalar(SQLStr, ConnectStr);
        }

        #region 报文加密解密
        public static string EncodeString(string DecodeResponseXML, string responseXML, string responseName)
        {
            string CompressType = "None";
            if (responseXML.IndexOf("<CompressType>Snappy</CompressType>") != -1)
                CompressType = "Snappy";
            else if (responseXML.IndexOf("<CompressType>Gzip</CompressType>") != -1)
                CompressType = "Gzip";

            if (responseName == "FlightIntl.Aggregator.Search.SearchFlights")
                CompressType = "FastInfo";



            string responsBody = null;
            try
            {
                if (CompressType == "FastInfo")
                {
                    return Serialize(DecodeResponseXML);
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    FlightResponse flightResponse = (FlightResponse)XMLSerializer.DeSerialize(DecodeResponseXML, typeof(FlightResponse));
                    ProtoBuf.Serializer.Serialize<FlightResponse>(ms, flightResponse);
                    ms.Position = 0;
                    byte[] responsBodyByte = ms.ToArray();
                    switch (CompressType)
                    {
                        case "None":
                            //responsBodyByte = responsBodyByte;
                            break;
                        case "Gzip":
                            responsBodyByte = CompressHelper.CompressByte2Byte(responsBodyByte);
                            break;
                        case "Snappy":
                            responsBodyByte = CompressTool.Compress(responsBodyByte);
                            break;
                        default:
                            //responsBodyByte = responsBodyByte;
                            break;
                    }
                    responsBody = Convert.ToBase64String(responsBodyByte);

                }
            }
            catch (Exception ex)
            {

            }

            return responsBody;
        }

        public static string DecodeString(string responseXML, string responseName, ref bool needEncode)
        {
            string responseBody = "";
            FlightResponse flightResponse = null;
            string CompressType = "None";
            if (responseXML.IndexOf("<CompressType>Snappy</CompressType>") != -1)
                CompressType = "Snappy";
            else if (responseXML.IndexOf("<CompressType>Gzip</CompressType>") != -1)
                CompressType = "Gzip";
            if(responseName == "FlightIntl.Aggregator.Search.SearchFlights")
                CompressType = "FastInfo";

            int length = 8;
            int start = responseXML.IndexOf("<string>");
            int end = responseXML.LastIndexOf("</string>");
            if (start == end)
            {
                start = responseXML.IndexOf("<Result>");
                end = responseXML.LastIndexOf("</Result>");
            }
            if (start == end)
            {
                start = responseXML.IndexOf("<q1:Result>");
                end = responseXML.LastIndexOf("</q1:Result>");
                length = 11;
            }
            if (start == end)
            {
                start = responseXML.IndexOf("<BodyEncoded>");
                end = responseXML.LastIndexOf("</BodyEncoded>");
                length = 13;
            }
            

            if (start != -1)
                responseBody = responseXML.Substring(start + length, end - start - length);
            else
                responseBody = responseXML;

            if (CompressType == "FastInfo")
            {
                return DeSerialize(responseBody);
            }

            string body = string.Empty;
            if (responseName == "政策查询接口" && start == -1 && end == -1)
            {
                needEncode = false;
                return responseBody;
            }
            byte[] responseBodyByte = Convert.FromBase64String(responseBody);
            switch (CompressType)
            {
                case "None":
                    //responseBodyByte = responseBodyByte;
                    break;
                case "Gzip":
                    responseBodyByte = CompressHelper.DecompressByte2Byte(responseBodyByte);
                    break;
                case "Snappy":
                    responseBodyByte = CompressTool.Uncompress(responseBodyByte);
                    break;
                default:
                    //responseBodyByte = responseBodyByte;
                    break;
            }

            using (MemoryStream ms = new MemoryStream(responseBodyByte))
            {
                flightResponse = ProtoBuf.Serializer.Deserialize<FlightResponse>(ms);
                body = XMLSerializer.Serialize(flightResponse);


            }
            return body;
        }

        private static FastInfosetCompression compression = FastInfosetCompression.GZip;
        //请求格式
        public static RequestFormat FormatStrType { get; set; }
        /// <summary>
        /// 请求格式类型的枚举
        /// </summary>
        public enum RequestFormat
        {
            XML,
            JSON,
            SOAP
        }

        //加密FI
        public static string Serialize(string xmlString)
        {
            try
            {
                XmlReader reader = new XmlTextReader(new MemoryStream(Encoding.UTF8.GetBytes(xmlString)));
                MemoryStream stream = new MemoryStream(64 * 1024);
                StringBuilder sb = new StringBuilder();
                using (XmlWriter writer = new XmlFastInfosetWriter(stream, compression, 6))
                {
                    XmlSchemaBasedConverter converter = new XmlSchemaBasedConverter(null, null);
                    converter.Convert(writer, reader);
                    XmlWriter writerXml = new XmlTextWriter(new StringWriter(sb));
                    writerXml.WriteBase64(stream.GetBuffer(), 0, (int)stream.Length);
                    writerXml.Flush();
                }
                reader.Close();
                return sb.ToString();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
        //解密 FastInfo -> XML
        public static string DeSerialize(string fiString)
        {
            try
            {
                XmlFastInfosetReader reader = new XmlFastInfosetReader(new MemoryStream(Convert.FromBase64String(fiString)), compression);
                StringBuilder sb = new StringBuilder();
                using (XmlWriter writer = new XmlTextWriter(new StringWriter(sb)) { Formatting = Formatting.Indented })
                {
                    XmlSchemaBasedConverter converter = new XmlSchemaBasedConverter(null, null);
                    converter.Convert(writer, reader);
                }
                reader.Close();
                return sb.ToString();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
       

        // Gzip格式解压缩
        public static string GZipDecompressString(string zippedString)
        {
            if (string.IsNullOrEmpty(zippedString) || zippedString.Length == 0)
            {
                return "";
            }
            else
            {
                byte[] zippedData = Convert.FromBase64String(zippedString.ToString());
                return (string)(System.Text.Encoding.UTF8.GetString(Decompress(zippedData)));
            }
        }

        // Gzip格式解压缩
        public static byte[] Decompress(byte[] zippedData)
        {
            MemoryStream ms = new MemoryStream(zippedData);
            GZipStream compressedzipStream = new GZipStream(ms, CompressionMode.Decompress);
            MemoryStream outBuffer = new MemoryStream();
            byte[] block = new byte[1024];
            while (true)
            {
                int bytesRead = compressedzipStream.Read(block, 0, block.Length);
                if (bytesRead <= 0)
                    break;
                else
                    outBuffer.Write(block, 0, bytesRead);
            }
            compressedzipStream.Close();
            return outBuffer.ToArray();
        }

        #endregion


        #region 数据库相关
        // <summary>
        /// 执行SQL语句
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>返回数据表</returns>
        public static DataTable ExecuteSql(string SQLString, string connStr)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                using (SqlCommand cmd = new SqlCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        cmd.CommandTimeout = 6000;
                        SqlDataAdapter sda = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        sda.Fill(ds);
                        return ds.Tables[0];
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        connection.Close();
                        throw e;
                    }
                }
            }
        }

        /// <summary>
        /// 执行SQL语句，返回结果
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>返回第一个数据</returns>
        public static object ExecuteSqlByScalar(string SQLString, string connStr)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                using (SqlCommand cmd = new SqlCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        cmd.CommandTimeout = 6000;
                        object rows = cmd.ExecuteScalar();
                        return rows;
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        connection.Close();
                        throw e;
                    }
                }
            }
        }

        #endregion


    }
}
