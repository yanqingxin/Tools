﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace ConfigFileModifier
{
    class ConfigFileModifier
    {
        public string env = string.Empty;
        public string path = string.Empty;
        static void Main(string[] args)
        {
            ConfigFileModifier cfM = new ConfigFileModifier();
            cfM.log("开始检查#############################################" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
            foreach (string eachLine in File.ReadAllLines(System.AppDomain.CurrentDomain.BaseDirectory + "\\Config.ini", Encoding.Default))
            {
                cfM.path = eachLine.Split('|')[0];
                cfM.env = eachLine.Split('|')[1];

                cfM.findConfigDir(cfM.path);
            }
            cfM.log("结束检查#############################################" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
        }

        /// <summary>
        /// 寻找当前路径下所有的Config目录，修改Appsetting
        /// </summary>
        /// <param name="path"></param>
        public void findConfigDir(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            DirectoryInfo[] diA = di.GetDirectories();
            foreach(DirectoryInfo eachDir in diA)
            {
                bool isEditAppSetting = false;//是否需要更新WebConfig
                if (eachDir.Name.ToUpper() == "CONFIG")
                {

                    foreach (FileInfo eachFile in eachDir.GetFiles())
                    {
                        if (eachFile.Name.ToUpper() == "APPSETTING.CONFIG" || eachFile.Name.ToUpper() == "APPSETTINGS.CONFIG")
                        {
                            isEditAppSetting = EditAppSetting(eachFile.FullName);
                            if (isEditAppSetting)
                                log("更新Appsetting文件：" + eachFile.FullName);
                            break;
                        }
                    }
                }

                if (isEditAppSetting)
                {
                    foreach (FileInfo eachFile in eachDir.Parent.GetFiles())
                    {
                        if (eachFile.Name.ToUpper() == "WEB.CONFIG")
                        {
                            if (EditWebConfig(eachFile.FullName))
                                log("更新WebConfig文件：" + eachFile.FullName);
                        }
                    }
                }

                if (eachDir.Name.ToUpper() != "CONFIG" && eachDir.Name.ToUpper() != "__CONFIGTEMP")
                    findConfigDir(eachDir.FullName);
            }


        }

        public bool EditWebConfig(string path)
        {
            try
            {
                string webConfig = File.ReadAllText(path);
                File.WriteAllText(path, webConfig.Replace(">", "> "));
            }
            catch (Exception e)
            {
                log("更新WebConfig文件失败：" + e.Message);
                return false;
            }
            return true;
        }

        public bool EditAppSetting(string path)
        {
            string appsetting = File.ReadAllText(path);
            MatchCollection match =  Regex.Matches(appsetting, "<add( )*key=\"SOA.ServiceRegistry.TestSubEnv.*value=\"[a-zA-Z0-9]+\"/>", RegexOptions.IgnoreCase);
            string configStr = ConfigFile(env);
            //if (match.Count > 0 && match.Groups[0].Value != string.Empty)
            if(!appsetting.Contains(configStr))
            {
                //if (!match.Groups[0].Value.Contains(env))
                //{
                //    //appsetting = appsetting.Replace(match.Groups[0].Value, string.Format("<add key=\"SOA.ServiceRegistry.TestSubEnv\" value=\"{0}\"/>", env));
                //    appsetting = appsetting.Replace(match.Groups[0].Value, configStr);
                //}
                //else
                //    return false;
                foreach (Match eachMatch in match)
                {
                    appsetting=appsetting.Replace(eachMatch.Groups[0].Value, "");
                }
                appsetting = appsetting.Replace("<appSettings>", string.Format("<appSettings>\r\n{0}", configStr));
            }
            else
            {
                return false;
            }
            File.WriteAllText(path, appsetting);
            return true;
        }

        public string ConfigFile(string env)
        {
            string configStr = string.Format(@"<add key=""SOA.ServiceRegistry.TestSubEnv:CompetitorPriceSearch{{http://soa.ctrip.com/flight/product/CompetitorPriceSearchService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:TransportSearchAPI{{http://soa.ctrip.com/flight/product/TransportSearchAPI/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightCacheSearch{{http://soa.ctrip.com/flight/product/FlightCacheSearch/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightInsuranceAPI{{http://soa.ctrip.com/flight/product/FlightInsuranceAPIService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightInsuranceService{{http://soa.ctrip.com/flight/Order/FlightInsuranceService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightMockService{{http://soa.ctrip.com/flight/Order/FlightMockService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightProductWS{{http://soa.ctrip.com/flight/product/FlightProductWS/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightSearchWS{{http://soa.ctrip.com/flight/product/FlightSearchWS/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:InsuranceAPI{{http://soa.ctrip.com/flight/product/aggregator/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:OrderProcess{{http://soa.ctrip.com/flight/Order/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:TradeQuickAPIService{{http://soa.ctrip.com/flight/tradequickapi/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:CertificateImageProcess{{http://soa.ctrip.com/flight/Order/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:InvoiceInfoService{{http://soa.ctrip.com/flight/Order/InvoiceInfoService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:OpenApiSegment{{http://soa.ctrip.com/flight/Ticket/OpenApiSegment/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:SendCouponService{{http://soa.ctrip.com/flight/Order/SendCouponService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:UserProfile{{http://soa.ctrip.com/platform/members/UserProfile/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:OrderIndexSearch{{http://soa.ctrip.com/flight/Order/OrderIndexSearch/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightSearchWS{{http://soa.ctrip.com/flight/product/FlightSearchWS/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:XProductsDetailService{{http://soa.ctrip.com/flight/aggregator/XProductsDetailService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:OrderProcess{{http://soa.ctrip.com/flight/Order/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:verifybooking{{http://soa.ctrip.com/flight/product/verifybooking/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightChangeService{{http://soa.ctrip.com/flight/Order/FlightChangeProcess/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightMockService{{http://soa.ctrip.com/flight/Order/FlightMockService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:OpenApiSegment{{http://soa.ctrip.com/flight/Ticket/OpenApiSegment/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:afterservice_checkin{{http://soa.ctrip.com/flight/wireless/afterservice/checkin/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:InvoiceInfoService{{http://soa.ctrip.com/flight/Order/InvoiceInfoService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:promocodeService{{http://soa.ctrip.com/platform/account/promocodeService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:OpenApiExchange{{http://soa.ctrip.com/flight/Ticket/OpenApiExchange/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightOrderRebooking{{http://soa.ctrip.com/flight/Order/FlightOrderRebooking/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:CheckInService{{http://soa.ctrip.com/flight/Order/CheckInService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:verifybooking{{http://soa.ctrip.com/flight/product/verifybooking/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:CompetitorPriceSearch{{http://soa.ctrip.com/flight/product/CompetitorPriceSearchService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:OrderDetailSearch{{http://soa.ctrip.com/flight/Order/OrderDetailSearch/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:BookSeatService{{http://soa.ctrip.com/flight/Order/BookSeatService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:CoreTransportSearchAPI{{http://soa.ctrip.com/flight/product/TransportSearchAPI/v2}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightCacheSearch{{http://soa.ctrip.com/flight/product/FlightCacheSearch/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:PredictingUserBehavior{{http://soa.ctrip.com/flight/Order/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:ProcessApiService{{http://soa.ctrip.com/flight/Order/ProcessApiService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:FlightCoreSearch{{http://soa.ctrip.com/flight/product/FlightCoreSearch/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:OrderTicketService{{http://soa.ctrip.com/flight/Order/OrderTicketService/v1}}"" value=""{0}""/>
<add key=""SOA.ServiceRegistry.TestSubEnv:TripServicePlatformService{{http://soa.ctrip.com/tour/TourSystem/TripServicePlatform/v1}}"" value=""{0}""/>", env);
return configStr;
        }


        /// <summary>
        /// 记录日志到本地
        /// </summary>
        /// <param name="message"></param>
        public void log(string message)
        {
            FileStream fs = new FileStream("C:\\Log4Modifier.txt", FileMode.Append);
            StreamWriter sw = new StreamWriter(fs, Encoding.Default);
            sw.WriteLine(message);
            sw.Close();
            fs.Close(); 
        }

    }
}
